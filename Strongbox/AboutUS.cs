﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Strongbox
{
    public partial class AboutUS : Form
    {
        Utils u = new Utils();

        public AboutUS()
        {
            InitializeComponent();
            this.BackgroundImage = u.GetImageBySkin("pix");
            logo.Image = u.GetImage("yslogo");
            close_box.Image = u.GetImageBySkin("close");
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://yisin.88448.com");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void AboutUS_Load(object sender, EventArgs e)
        {
            label3.Text = "当前版本：" + Form1.appVersion;

            Color col = Color.FromArgb(255, 247, 247, 247);
            panel1.BackColor = col;
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.yinsin.net");
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("mailto:yinsin@yinsin.net");
        }

        private void pictureBox2_MouseEnter(object sender, EventArgs e)
        {
            PictureBox box = (PictureBox)sender;
            String tag = box.Tag.ToString();
            box.Image = u.GetImageBySkin(tag, 1);
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            PictureBox box = (PictureBox)sender;
            String tag = box.Tag.ToString();
            box.Image = u.GetImageBySkin(tag, 0);
        }

        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                PictureBox box = (PictureBox)sender;
                String tag = box.Tag.ToString();
                box.Image = u.GetImageBySkin(tag, 2);
            }
        }

        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox box = (PictureBox)sender;
            String tag = box.Tag.ToString();
            box.Image = u.GetImageBySkin(tag, 1);
        }

        Point mouseOff;//鼠标移动位置变量
        bool leftFlag;//标签是否为左键
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y); //得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }
        }

    }
}
