﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Strongbox
{
    public partial class Feedback : Form
    {
        Utils u = new Utils();
        public Feedback()
        {
            InitializeComponent();
            this.BackgroundImage = u.GetImageBySkin("pix");
            close_box.Image = u.GetImageBySkin("close");
        }

        private void Feedback_Load(object sender, EventArgs e)
        {
            webBrowser1.Url = new Uri("http://mail.qq.com/cgi-bin/qm_share?t=qm_mailme&email=D3ZmYXdmYTw8PTlPeWZ-IX5_IWxgYg");
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            webBrowser1.ScrollBarsEnabled = false;
        }


        private void pictureBox2_MouseEnter(object sender, EventArgs e)
        {
            PictureBox box = (PictureBox)sender;
            String tag = box.Tag.ToString();
            box.Image = u.GetImageBySkin(tag, 1);
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            PictureBox box = (PictureBox)sender;
            String tag = box.Tag.ToString();
            box.Image = u.GetImageBySkin(tag);
        }

        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                PictureBox box = (PictureBox)sender;
                String tag = box.Tag.ToString();
                box.Image = u.GetImageBySkin(tag, 2);
            }
        }

        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox box = (PictureBox)sender;
            String tag = box.Tag.ToString();
            box.Image = u.GetImageBySkin(tag, 1);
        }

        Point mouseOff;//鼠标移动位置变量
        bool leftFlag;//标签是否为左键
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y); //得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

    }
}
