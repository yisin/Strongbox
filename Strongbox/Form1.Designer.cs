﻿using System.Drawing;

namespace Strongbox
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.searchKey = new System.Windows.Forms.TextBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.tree_contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menu_addGenre = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.menu_add_son_genre = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_update_item = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_del_item = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.展开全部ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.收缩全部ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_up_move = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_down_move = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.menu_get_icon = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_set_local = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_reset_icon = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.操作ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.添加分类ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.保存ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.锁定ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.选项ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.官网ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.treeMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.添加分类ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.添加子类ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.修改名称ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.上移ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.下移ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.获取图标ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.更改图标ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.还原默认图标ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel2 = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.textBox_adress = new System.Windows.Forms.TextBox();
            this.textBox_name = new System.Windows.Forms.TextBox();
            this.rich_back = new System.Windows.Forms.PictureBox();
            this.goto_box = new System.Windows.Forms.PictureBox();
            this.sync_box = new System.Windows.Forms.PictureBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.copyacc5 = new System.Windows.Forms.PictureBox();
            this.showPBox5 = new System.Windows.Forms.PictureBox();
            this.copypwd5 = new System.Windows.Forms.PictureBox();
            this.password5 = new System.Windows.Forms.TextBox();
            this.account5 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.copyacc4 = new System.Windows.Forms.PictureBox();
            this.showPBox4 = new System.Windows.Forms.PictureBox();
            this.copypwd4 = new System.Windows.Forms.PictureBox();
            this.password4 = new System.Windows.Forms.TextBox();
            this.account4 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.copyacc2 = new System.Windows.Forms.PictureBox();
            this.showPBox2 = new System.Windows.Forms.PictureBox();
            this.copypwd2 = new System.Windows.Forms.PictureBox();
            this.password2 = new System.Windows.Forms.TextBox();
            this.account2 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.copyacc3 = new System.Windows.Forms.PictureBox();
            this.showPBox3 = new System.Windows.Forms.PictureBox();
            this.copypwd3 = new System.Windows.Forms.PictureBox();
            this.password3 = new System.Windows.Forms.TextBox();
            this.account3 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.copyacc1 = new System.Windows.Forms.PictureBox();
            this.showPBox1 = new System.Windows.Forms.PictureBox();
            this.copypwd1 = new System.Windows.Forms.PictureBox();
            this.password1 = new System.Windows.Forms.TextBox();
            this.account1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel4 = new System.Windows.Forms.Panel();
            this.info_label = new System.Windows.Forms.Label();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.icon_contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.显示窗口ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.锁定ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.close_box = new System.Windows.Forms.PictureBox();
            this.min_box = new System.Windows.Forms.PictureBox();
            this.menu_box = new System.Windows.Forms.PictureBox();
            this.skin_box = new System.Windows.Forms.PictureBox();
            this.feedback_box = new System.Windows.Forms.PictureBox();
            this.logo = new System.Windows.Forms.PictureBox();
            this.title1_pox = new System.Windows.Forms.PictureBox();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.锁定保险箱ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.开机启动ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.修改密码ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.帮助ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.关于ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.使用说明ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.官方论坛ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.保存数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.备份数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.还原数据ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.工具ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.加密文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.文件解密ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.退出ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.title2_box = new System.Windows.Forms.PictureBox();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.panel_skin = new System.Windows.Forms.Panel();
            this.choosebox_silver = new System.Windows.Forms.PictureBox();
            this.choosebox_blank = new System.Windows.Forms.PictureBox();
            this.choosebox_red = new System.Windows.Forms.PictureBox();
            this.choosebox_blue = new System.Windows.Forms.PictureBox();
            this.choosebox_green = new System.Windows.Forms.PictureBox();
            this.boxskin_silver = new System.Windows.Forms.PictureBox();
            this.boxskin_blank = new System.Windows.Forms.PictureBox();
            this.boxskin_red = new System.Windows.Forms.PictureBox();
            this.boxskin_blue = new System.Windows.Forms.PictureBox();
            this.boxskin_green = new System.Windows.Forms.PictureBox();
            this.version = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.tree_contextMenu.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rich_back)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.goto_box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sync_box)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copyacc5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showPBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.copypwd5)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copyacc4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showPBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.copypwd4)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copyacc2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showPBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.copypwd2)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copyacc3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showPBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.copypwd3)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copyacc1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showPBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.copypwd1)).BeginInit();
            this.panel4.SuspendLayout();
            this.icon_contextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.close_box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.min_box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.menu_box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.skin_box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.feedback_box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.title1_pox)).BeginInit();
            this.contextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.title2_box)).BeginInit();
            this.panel_skin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.choosebox_silver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.choosebox_blank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.choosebox_red)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.choosebox_blue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.choosebox_green)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxskin_silver)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxskin_blank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxskin_red)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxskin_blue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxskin_green)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.searchKey);
            this.panel1.Controls.Add(this.treeView1);
            this.panel1.Location = new System.Drawing.Point(1, 57);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(308, 542);
            this.panel1.TabIndex = 0;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("方正咆哮简体", 16F);
            this.label15.ForeColor = System.Drawing.Color.Olive;
            this.label15.Location = new System.Drawing.Point(15, 44);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(276, 28);
            this.label15.TabIndex = 4;
            this.label15.Text = "右键点击这块区域有菜单哦";
            this.label15.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.label12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label12.Font = new System.Drawing.Font("宋体", 18F);
            this.label12.ForeColor = System.Drawing.Color.Green;
            this.label12.Location = new System.Drawing.Point(277, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(22, 24);
            this.label12.TabIndex = 3;
            this.label12.Text = "+";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            this.label12.MouseEnter += new System.EventHandler(this.treetool_MouseEnter);
            this.label12.MouseLeave += new System.EventHandler(this.treetool_MouseLeave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.label11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label11.Font = new System.Drawing.Font("宋体", 18F);
            this.label11.ForeColor = System.Drawing.Color.Green;
            this.label11.Location = new System.Drawing.Point(252, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(22, 24);
            this.label11.TabIndex = 2;
            this.label11.Text = "-";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            this.label11.MouseEnter += new System.EventHandler(this.treetool_MouseEnter);
            this.label11.MouseLeave += new System.EventHandler(this.treetool_MouseLeave);
            // 
            // searchKey
            // 
            this.searchKey.Font = new System.Drawing.Font("宋体", 10F);
            this.searchKey.Location = new System.Drawing.Point(6, 5);
            this.searchKey.Name = "searchKey";
            this.searchKey.Size = new System.Drawing.Size(239, 23);
            this.searchKey.TabIndex = 1;
            this.searchKey.KeyUp += new System.Windows.Forms.KeyEventHandler(this.searchKey_KeyUp);
            // 
            // treeView1
            // 
            this.treeView1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.treeView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeView1.ContextMenuStrip = this.tree_contextMenu;
            this.treeView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.treeView1.ImageIndex = 0;
            this.treeView1.ImageList = this.imageList1;
            this.treeView1.Location = new System.Drawing.Point(6, 31);
            this.treeView1.Name = "treeView1";
            this.treeView1.SelectedImageIndex = 0;
            this.treeView1.Size = new System.Drawing.Size(296, 506);
            this.treeView1.StateImageList = this.imageList1;
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.treeView1_AfterLabelEdit);
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            this.treeView1.Enter += new System.EventHandler(this.treeView1_Enter);
            this.treeView1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form_KeyUp);
            this.treeView1.Leave += new System.EventHandler(this.treeView1_Leave);
            this.treeView1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.treeView1_MouseDoubleClick);
            this.treeView1.MouseLeave += new System.EventHandler(this.treeView1_MouseLeave);
            this.treeView1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.treeView1_MouseMove);
            this.treeView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.treeView1_MouseUp);
            // 
            // tree_contextMenu
            // 
            this.tree_contextMenu.BackColor = System.Drawing.Color.White;
            this.tree_contextMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tree_contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_addGenre,
            this.toolStripSeparator4,
            this.menu_add_son_genre,
            this.menu_update_item,
            this.menu_del_item,
            this.toolStripSeparator5,
            this.展开全部ToolStripMenuItem,
            this.收缩全部ToolStripMenuItem,
            this.menu_up_move,
            this.menu_down_move,
            this.toolStripSeparator6,
            this.menu_get_icon,
            this.menu_set_local,
            this.menu_reset_icon});
            this.tree_contextMenu.Name = "tree_contextMenu";
            this.tree_contextMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.tree_contextMenu.Size = new System.Drawing.Size(216, 264);
            // 
            // menu_addGenre
            // 
            this.menu_addGenre.Name = "menu_addGenre";
            this.menu_addGenre.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.menu_addGenre.Size = new System.Drawing.Size(215, 22);
            this.menu_addGenre.Text = "添加分类";
            this.menu_addGenre.Click += new System.EventHandler(this.添加分类ToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(212, 6);
            // 
            // menu_add_son_genre
            // 
            this.menu_add_son_genre.Name = "menu_add_son_genre";
            this.menu_add_son_genre.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.menu_add_son_genre.Size = new System.Drawing.Size(215, 22);
            this.menu_add_son_genre.Text = "添加子类";
            this.menu_add_son_genre.Click += new System.EventHandler(this.menu_add_son_genre_Click);
            // 
            // menu_update_item
            // 
            this.menu_update_item.Name = "menu_update_item";
            this.menu_update_item.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.menu_update_item.Size = new System.Drawing.Size(215, 22);
            this.menu_update_item.Text = "修改名称";
            this.menu_update_item.Click += new System.EventHandler(this.menu_update_item_Click);
            // 
            // menu_del_item
            // 
            this.menu_del_item.Name = "menu_del_item";
            this.menu_del_item.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.menu_del_item.Size = new System.Drawing.Size(215, 22);
            this.menu_del_item.Text = "删除此项";
            this.menu_del_item.Click += new System.EventHandler(this.menu_del_item_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(212, 6);
            // 
            // 展开全部ToolStripMenuItem
            // 
            this.展开全部ToolStripMenuItem.Name = "展开全部ToolStripMenuItem";
            this.展开全部ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.OemMinus)));
            this.展开全部ToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.展开全部ToolStripMenuItem.Text = "展开全部";
            this.展开全部ToolStripMenuItem.Click += new System.EventHandler(this.展开所有ToolStripMenuItem_Click);
            // 
            // 收缩全部ToolStripMenuItem
            // 
            this.收缩全部ToolStripMenuItem.Name = "收缩全部ToolStripMenuItem";
            this.收缩全部ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Oemplus)));
            this.收缩全部ToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.收缩全部ToolStripMenuItem.Text = "收缩全部";
            this.收缩全部ToolStripMenuItem.Click += new System.EventHandler(this.收缩所有ToolStripMenuItem_Click);
            // 
            // menu_up_move
            // 
            this.menu_up_move.Name = "menu_up_move";
            this.menu_up_move.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Up)));
            this.menu_up_move.Size = new System.Drawing.Size(215, 22);
            this.menu_up_move.Text = "上移";
            this.menu_up_move.Click += new System.EventHandler(this.menu_up_move_Click);
            // 
            // menu_down_move
            // 
            this.menu_down_move.Name = "menu_down_move";
            this.menu_down_move.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Down)));
            this.menu_down_move.Size = new System.Drawing.Size(215, 22);
            this.menu_down_move.Text = "下移";
            this.menu_down_move.Click += new System.EventHandler(this.menu_down_move_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(212, 6);
            // 
            // menu_get_icon
            // 
            this.menu_get_icon.Name = "menu_get_icon";
            this.menu_get_icon.Size = new System.Drawing.Size(215, 22);
            this.menu_get_icon.Text = "获取网络图标";
            this.menu_get_icon.Click += new System.EventHandler(this.menu_get_icon_Click);
            // 
            // menu_set_local
            // 
            this.menu_set_local.Name = "menu_set_local";
            this.menu_set_local.Size = new System.Drawing.Size(215, 22);
            this.menu_set_local.Text = "设置本地图标";
            this.menu_set_local.Click += new System.EventHandler(this.menu_set_local_Click);
            // 
            // menu_reset_icon
            // 
            this.menu_reset_icon.Name = "menu_reset_icon";
            this.menu_reset_icon.Size = new System.Drawing.Size(215, 22);
            this.menu_reset_icon.Text = "还原默认图标";
            this.menu_reset_icon.Click += new System.EventHandler(this.menu_reset_icon_Click);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // 操作ToolStripMenuItem
            // 
            this.操作ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.添加分类ToolStripMenuItem,
            this.保存ToolStripMenuItem,
            this.退出ToolStripMenuItem});
            this.操作ToolStripMenuItem.Name = "操作ToolStripMenuItem";
            this.操作ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.操作ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.操作ToolStripMenuItem.Text = "操作";
            // 
            // 添加分类ToolStripMenuItem
            // 
            this.添加分类ToolStripMenuItem.Name = "添加分类ToolStripMenuItem";
            this.添加分类ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.添加分类ToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.添加分类ToolStripMenuItem.Text = "添加分类";
            this.添加分类ToolStripMenuItem.Click += new System.EventHandler(this.添加分类ToolStripMenuItem_Click);
            // 
            // 保存ToolStripMenuItem
            // 
            this.保存ToolStripMenuItem.Name = "保存ToolStripMenuItem";
            this.保存ToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.退出ToolStripMenuItem.Text = "退出";
            // 
            // 设置ToolStripMenuItem
            // 
            this.设置ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.锁定ToolStripMenuItem,
            this.选项ToolStripMenuItem});
            this.设置ToolStripMenuItem.Name = "设置ToolStripMenuItem";
            this.设置ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.设置ToolStripMenuItem.Text = "设置";
            // 
            // 锁定ToolStripMenuItem
            // 
            this.锁定ToolStripMenuItem.Name = "锁定ToolStripMenuItem";
            this.锁定ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.锁定ToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.锁定ToolStripMenuItem.Text = "锁定";
            // 
            // 选项ToolStripMenuItem
            // 
            this.选项ToolStripMenuItem.Name = "选项ToolStripMenuItem";
            this.选项ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.选项ToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.选项ToolStripMenuItem.Text = "选项";
            // 
            // 关于ToolStripMenuItem
            // 
            this.关于ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.关于ToolStripMenuItem1,
            this.帮助ToolStripMenuItem,
            this.官网ToolStripMenuItem});
            this.关于ToolStripMenuItem.Name = "关于ToolStripMenuItem";
            this.关于ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.关于ToolStripMenuItem.Text = "关于";
            // 
            // 关于ToolStripMenuItem1
            // 
            this.关于ToolStripMenuItem1.Name = "关于ToolStripMenuItem1";
            this.关于ToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.J)));
            this.关于ToolStripMenuItem1.Size = new System.Drawing.Size(146, 22);
            this.关于ToolStripMenuItem1.Text = "关于";
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.帮助ToolStripMenuItem.Text = "帮助";
            // 
            // 官网ToolStripMenuItem
            // 
            this.官网ToolStripMenuItem.Name = "官网ToolStripMenuItem";
            this.官网ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.官网ToolStripMenuItem.Text = "官网";
            // 
            // treeMenuToolStripMenuItem
            // 
            this.treeMenuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.添加分类ToolStripMenuItem1,
            this.toolStripSeparator2,
            this.添加子类ToolStripMenuItem,
            this.修改名称ToolStripMenuItem,
            this.删除ToolStripMenuItem,
            this.toolStripSeparator1,
            this.上移ToolStripMenuItem,
            this.下移ToolStripMenuItem,
            this.toolStripSeparator3,
            this.获取图标ToolStripMenuItem,
            this.更改图标ToolStripMenuItem,
            this.还原默认图标ToolStripMenuItem});
            this.treeMenuToolStripMenuItem.Name = "treeMenuToolStripMenuItem";
            this.treeMenuToolStripMenuItem.Size = new System.Drawing.Size(76, 21);
            this.treeMenuToolStripMenuItem.Text = "treeMenu";
            this.treeMenuToolStripMenuItem.Visible = false;
            // 
            // 添加分类ToolStripMenuItem1
            // 
            this.添加分类ToolStripMenuItem1.Name = "添加分类ToolStripMenuItem1";
            this.添加分类ToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.添加分类ToolStripMenuItem1.Size = new System.Drawing.Size(193, 22);
            this.添加分类ToolStripMenuItem1.Text = "添加分类";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(190, 6);
            // 
            // 添加子类ToolStripMenuItem
            // 
            this.添加子类ToolStripMenuItem.Name = "添加子类ToolStripMenuItem";
            this.添加子类ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this.添加子类ToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.添加子类ToolStripMenuItem.Text = "添加子类";
            // 
            // 修改名称ToolStripMenuItem
            // 
            this.修改名称ToolStripMenuItem.Name = "修改名称ToolStripMenuItem";
            this.修改名称ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.修改名称ToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.修改名称ToolStripMenuItem.Text = "修改";
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.删除ToolStripMenuItem.Text = "删除";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(190, 6);
            // 
            // 上移ToolStripMenuItem
            // 
            this.上移ToolStripMenuItem.Name = "上移ToolStripMenuItem";
            this.上移ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.上移ToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.上移ToolStripMenuItem.Text = "上移";
            // 
            // 下移ToolStripMenuItem
            // 
            this.下移ToolStripMenuItem.Name = "下移ToolStripMenuItem";
            this.下移ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.K)));
            this.下移ToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.下移ToolStripMenuItem.Text = "下移";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(190, 6);
            // 
            // 获取图标ToolStripMenuItem
            // 
            this.获取图标ToolStripMenuItem.Name = "获取图标ToolStripMenuItem";
            this.获取图标ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.获取图标ToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.获取图标ToolStripMenuItem.Text = "获取图标";
            // 
            // 更改图标ToolStripMenuItem
            // 
            this.更改图标ToolStripMenuItem.Name = "更改图标ToolStripMenuItem";
            this.更改图标ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.更改图标ToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.更改图标ToolStripMenuItem.Text = "更改图标";
            // 
            // 还原默认图标ToolStripMenuItem
            // 
            this.还原默认图标ToolStripMenuItem.Name = "还原默认图标ToolStripMenuItem";
            this.还原默认图标ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.还原默认图标ToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.还原默认图标ToolStripMenuItem.Text = "还原默认图标";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel2.Controls.Add(this.richTextBox1);
            this.panel2.Controls.Add(this.textBox_adress);
            this.panel2.Controls.Add(this.textBox_name);
            this.panel2.Controls.Add(this.rich_back);
            this.panel2.Controls.Add(this.goto_box);
            this.panel2.Controls.Add(this.sync_box);
            this.panel2.Controls.Add(this.groupBox5);
            this.panel2.Controls.Add(this.groupBox3);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox4);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Cursor = System.Windows.Forms.Cursors.Default;
            this.panel2.Location = new System.Drawing.Point(310, 57);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(589, 542);
            this.panel2.TabIndex = 3;
            this.panel2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseMove);
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Location = new System.Drawing.Point(17, 311);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(558, 218);
            this.richTextBox1.TabIndex = 9;
            this.richTextBox1.Text = "";
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            this.richTextBox1.Enter += new System.EventHandler(this.richTextBox1_Enter);
            this.richTextBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form_KeyUp);
            this.richTextBox1.Leave += new System.EventHandler(this.richTextBox1_Leave);
            this.richTextBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.richTextBox1_MouseMove);
            // 
            // textBox_adress
            // 
            this.textBox_adress.Location = new System.Drawing.Point(60, 38);
            this.textBox_adress.Name = "textBox_adress";
            this.textBox_adress.Size = new System.Drawing.Size(477, 21);
            this.textBox_adress.TabIndex = 11;
            this.textBox_adress.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
            this.textBox_adress.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form_KeyUp);
            // 
            // textBox_name
            // 
            this.textBox_name.Location = new System.Drawing.Point(60, 8);
            this.textBox_name.Name = "textBox_name";
            this.textBox_name.Size = new System.Drawing.Size(477, 21);
            this.textBox_name.TabIndex = 10;
            this.textBox_name.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
            this.textBox_name.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form_KeyUp);
            // 
            // rich_back
            // 
            this.rich_back.Location = new System.Drawing.Point(16, 310);
            this.rich_back.Name = "rich_back";
            this.rich_back.Size = new System.Drawing.Size(560, 220);
            this.rich_back.TabIndex = 26;
            this.rich_back.TabStop = false;
            // 
            // goto_box
            // 
            this.goto_box.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.goto_box.Cursor = System.Windows.Forms.Cursors.Hand;
            this.goto_box.Location = new System.Drawing.Point(551, 35);
            this.goto_box.Name = "goto_box";
            this.goto_box.Size = new System.Drawing.Size(24, 24);
            this.goto_box.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.goto_box.TabIndex = 25;
            this.goto_box.TabStop = false;
            this.goto_box.Tag = "gonet";
            this.toolTip1.SetToolTip(this.goto_box, "若是网址，可以点击在浏览器中打开");
            this.goto_box.Click += new System.EventHandler(this.pictureBox2_Click);
            this.goto_box.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.goto_box.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.goto_box.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.goto_box.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // sync_box
            // 
            this.sync_box.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.sync_box.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sync_box.Location = new System.Drawing.Point(551, 5);
            this.sync_box.Name = "sync_box";
            this.sync_box.Size = new System.Drawing.Size(24, 24);
            this.sync_box.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.sync_box.TabIndex = 24;
            this.sync_box.TabStop = false;
            this.sync_box.Tag = "sync";
            this.toolTip1.SetToolTip(this.sync_box, "同步名称");
            this.sync_box.Click += new System.EventHandler(this.pictureBox1_Click);
            this.sync_box.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.sync_box.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.sync_box.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.sync_box.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.copyacc5);
            this.groupBox5.Controls.Add(this.showPBox5);
            this.groupBox5.Controls.Add(this.copypwd5);
            this.groupBox5.Controls.Add(this.password5);
            this.groupBox5.Controls.Add(this.account5);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.ForeColor = System.Drawing.Color.Teal;
            this.groupBox5.Location = new System.Drawing.Point(15, 260);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(560, 41);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "账户5";
            // 
            // copyacc5
            // 
            this.copyacc5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.copyacc5.Location = new System.Drawing.Point(249, 12);
            this.copyacc5.Name = "copyacc5";
            this.copyacc5.Size = new System.Drawing.Size(24, 24);
            this.copyacc5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.copyacc5.TabIndex = 23;
            this.copyacc5.TabStop = false;
            this.copyacc5.Tag = "copy";
            this.toolTip1.SetToolTip(this.copyacc5, "点击复制账号5");
            this.copyacc5.Click += new System.EventHandler(this.button_func);
            this.copyacc5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.copyacc5.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.copyacc5.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.copyacc5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // showPBox5
            // 
            this.showPBox5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.showPBox5.Location = new System.Drawing.Point(530, 12);
            this.showPBox5.Name = "showPBox5";
            this.showPBox5.Size = new System.Drawing.Size(24, 24);
            this.showPBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.showPBox5.TabIndex = 23;
            this.showPBox5.TabStop = false;
            this.showPBox5.Tag = "lamp";
            this.toolTip1.SetToolTip(this.showPBox5, "显示密码5为明文");
            this.showPBox5.Click += new System.EventHandler(this.button_func);
            this.showPBox5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.showPBox5.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.showPBox5.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.showPBox5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // copypwd5
            // 
            this.copypwd5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.copypwd5.Location = new System.Drawing.Point(498, 12);
            this.copypwd5.Name = "copypwd5";
            this.copypwd5.Size = new System.Drawing.Size(24, 24);
            this.copypwd5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.copypwd5.TabIndex = 22;
            this.copypwd5.TabStop = false;
            this.copypwd5.Tag = "copy";
            this.toolTip1.SetToolTip(this.copypwd5, "点击复制密码5");
            this.copypwd5.Click += new System.EventHandler(this.button_func);
            this.copypwd5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.copypwd5.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.copypwd5.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.copypwd5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // password5
            // 
            this.password5.Location = new System.Drawing.Point(314, 13);
            this.password5.Name = "password5";
            this.password5.PasswordChar = '◆';
            this.password5.Size = new System.Drawing.Size(177, 21);
            this.password5.TabIndex = 21;
            this.password5.Tag = "";
            this.password5.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
            this.password5.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form_KeyUp);
            // 
            // account5
            // 
            this.account5.Location = new System.Drawing.Point(45, 13);
            this.account5.Name = "account5";
            this.account5.Size = new System.Drawing.Size(197, 21);
            this.account5.TabIndex = 20;
            this.account5.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
            this.account5.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form_KeyUp);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label13.Location = new System.Drawing.Point(280, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 3;
            this.label13.Text = "密码：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label14.Location = new System.Drawing.Point(11, 19);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 12);
            this.label14.TabIndex = 2;
            this.label14.Text = "帐号：";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.copyacc4);
            this.groupBox3.Controls.Add(this.showPBox4);
            this.groupBox3.Controls.Add(this.copypwd4);
            this.groupBox3.Controls.Add(this.password4);
            this.groupBox3.Controls.Add(this.account4);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.ForeColor = System.Drawing.Color.Teal;
            this.groupBox3.Location = new System.Drawing.Point(15, 212);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(560, 41);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "账户4";
            // 
            // copyacc4
            // 
            this.copyacc4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.copyacc4.Location = new System.Drawing.Point(249, 12);
            this.copyacc4.Name = "copyacc4";
            this.copyacc4.Size = new System.Drawing.Size(24, 24);
            this.copyacc4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.copyacc4.TabIndex = 24;
            this.copyacc4.TabStop = false;
            this.copyacc4.Tag = "copy";
            this.toolTip1.SetToolTip(this.copyacc4, "点击复制账号4");
            this.copyacc4.Click += new System.EventHandler(this.button_func);
            this.copyacc4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.copyacc4.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.copyacc4.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.copyacc4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // showPBox4
            // 
            this.showPBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.showPBox4.Location = new System.Drawing.Point(530, 13);
            this.showPBox4.Name = "showPBox4";
            this.showPBox4.Size = new System.Drawing.Size(24, 24);
            this.showPBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.showPBox4.TabIndex = 21;
            this.showPBox4.TabStop = false;
            this.showPBox4.Tag = "lamp";
            this.toolTip1.SetToolTip(this.showPBox4, "显示密码4为明文");
            this.showPBox4.Click += new System.EventHandler(this.button_func);
            this.showPBox4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.showPBox4.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.showPBox4.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.showPBox4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // copypwd4
            // 
            this.copypwd4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.copypwd4.Location = new System.Drawing.Point(498, 13);
            this.copypwd4.Name = "copypwd4";
            this.copypwd4.Size = new System.Drawing.Size(24, 24);
            this.copypwd4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.copypwd4.TabIndex = 20;
            this.copypwd4.TabStop = false;
            this.copypwd4.Tag = "copy";
            this.toolTip1.SetToolTip(this.copypwd4, "点击复制密码4");
            this.copypwd4.Click += new System.EventHandler(this.button_func);
            this.copypwd4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.copypwd4.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.copypwd4.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.copypwd4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // password4
            // 
            this.password4.Location = new System.Drawing.Point(314, 14);
            this.password4.Name = "password4";
            this.password4.PasswordChar = '◆';
            this.password4.Size = new System.Drawing.Size(177, 21);
            this.password4.TabIndex = 19;
            this.password4.Tag = "";
            this.password4.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
            this.password4.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form_KeyUp);
            // 
            // account4
            // 
            this.account4.Location = new System.Drawing.Point(45, 14);
            this.account4.Name = "account4";
            this.account4.Size = new System.Drawing.Size(197, 21);
            this.account4.TabIndex = 18;
            this.account4.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
            this.account4.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form_KeyUp);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label7.Location = new System.Drawing.Point(280, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 3;
            this.label7.Text = "密码：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label8.Location = new System.Drawing.Point(11, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 2;
            this.label8.Text = "帐号：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.copyacc2);
            this.groupBox2.Controls.Add(this.showPBox2);
            this.groupBox2.Controls.Add(this.copypwd2);
            this.groupBox2.Controls.Add(this.password2);
            this.groupBox2.Controls.Add(this.account2);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.ForeColor = System.Drawing.Color.Teal;
            this.groupBox2.Location = new System.Drawing.Point(15, 117);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(560, 41);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "账户2";
            // 
            // copyacc2
            // 
            this.copyacc2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.copyacc2.Location = new System.Drawing.Point(249, 12);
            this.copyacc2.Name = "copyacc2";
            this.copyacc2.Size = new System.Drawing.Size(24, 24);
            this.copyacc2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.copyacc2.TabIndex = 26;
            this.copyacc2.TabStop = false;
            this.copyacc2.Tag = "copy";
            this.toolTip1.SetToolTip(this.copyacc2, "点击复制账号2");
            this.copyacc2.Click += new System.EventHandler(this.button_func);
            this.copyacc2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.copyacc2.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.copyacc2.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.copyacc2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // showPBox2
            // 
            this.showPBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.showPBox2.Location = new System.Drawing.Point(530, 12);
            this.showPBox2.Name = "showPBox2";
            this.showPBox2.Size = new System.Drawing.Size(24, 24);
            this.showPBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.showPBox2.TabIndex = 17;
            this.showPBox2.TabStop = false;
            this.showPBox2.Tag = "lamp";
            this.toolTip1.SetToolTip(this.showPBox2, "显示密码2为明文");
            this.showPBox2.Click += new System.EventHandler(this.button_func);
            this.showPBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.showPBox2.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.showPBox2.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.showPBox2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // copypwd2
            // 
            this.copypwd2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.copypwd2.Location = new System.Drawing.Point(498, 12);
            this.copypwd2.Name = "copypwd2";
            this.copypwd2.Size = new System.Drawing.Size(24, 24);
            this.copypwd2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.copypwd2.TabIndex = 16;
            this.copypwd2.TabStop = false;
            this.copypwd2.Tag = "copy";
            this.toolTip1.SetToolTip(this.copypwd2, "点击复制密码2");
            this.copypwd2.Click += new System.EventHandler(this.button_func);
            this.copypwd2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.copypwd2.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.copypwd2.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.copypwd2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // password2
            // 
            this.password2.Location = new System.Drawing.Point(314, 13);
            this.password2.Name = "password2";
            this.password2.PasswordChar = '◆';
            this.password2.Size = new System.Drawing.Size(177, 21);
            this.password2.TabIndex = 15;
            this.password2.Tag = "";
            this.password2.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
            this.password2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form_KeyUp);
            // 
            // account2
            // 
            this.account2.Location = new System.Drawing.Point(45, 13);
            this.account2.Name = "account2";
            this.account2.Size = new System.Drawing.Size(197, 21);
            this.account2.TabIndex = 14;
            this.account2.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
            this.account2.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form_KeyUp);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(280, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 3;
            this.label5.Text = "密码：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(11, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 2;
            this.label6.Text = "帐号：";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.copyacc3);
            this.groupBox4.Controls.Add(this.showPBox3);
            this.groupBox4.Controls.Add(this.copypwd3);
            this.groupBox4.Controls.Add(this.password3);
            this.groupBox4.Controls.Add(this.account3);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.ForeColor = System.Drawing.Color.Teal;
            this.groupBox4.Location = new System.Drawing.Point(15, 165);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(560, 41);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "账户3";
            // 
            // copyacc3
            // 
            this.copyacc3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.copyacc3.Location = new System.Drawing.Point(249, 12);
            this.copyacc3.Name = "copyacc3";
            this.copyacc3.Size = new System.Drawing.Size(24, 24);
            this.copyacc3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.copyacc3.TabIndex = 25;
            this.copyacc3.TabStop = false;
            this.copyacc3.Tag = "copy";
            this.toolTip1.SetToolTip(this.copyacc3, "点击复制账号3");
            this.copyacc3.Click += new System.EventHandler(this.button_func);
            this.copyacc3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.copyacc3.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.copyacc3.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.copyacc3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // showPBox3
            // 
            this.showPBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.showPBox3.Location = new System.Drawing.Point(530, 12);
            this.showPBox3.Name = "showPBox3";
            this.showPBox3.Size = new System.Drawing.Size(24, 24);
            this.showPBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.showPBox3.TabIndex = 19;
            this.showPBox3.TabStop = false;
            this.showPBox3.Tag = "lamp";
            this.toolTip1.SetToolTip(this.showPBox3, "显示密码3为明文");
            this.showPBox3.Click += new System.EventHandler(this.button_func);
            this.showPBox3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.showPBox3.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.showPBox3.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.showPBox3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // copypwd3
            // 
            this.copypwd3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.copypwd3.Location = new System.Drawing.Point(498, 12);
            this.copypwd3.Name = "copypwd3";
            this.copypwd3.Size = new System.Drawing.Size(24, 24);
            this.copypwd3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.copypwd3.TabIndex = 18;
            this.copypwd3.TabStop = false;
            this.copypwd3.Tag = "copy";
            this.toolTip1.SetToolTip(this.copypwd3, "点击复制密码3");
            this.copypwd3.Click += new System.EventHandler(this.button_func);
            this.copypwd3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.copypwd3.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.copypwd3.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.copypwd3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // password3
            // 
            this.password3.Location = new System.Drawing.Point(314, 14);
            this.password3.Name = "password3";
            this.password3.PasswordChar = '◆';
            this.password3.Size = new System.Drawing.Size(177, 21);
            this.password3.TabIndex = 17;
            this.password3.Tag = "";
            this.password3.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
            this.password3.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form_KeyUp);
            // 
            // account3
            // 
            this.account3.Location = new System.Drawing.Point(45, 14);
            this.account3.Name = "account3";
            this.account3.Size = new System.Drawing.Size(197, 21);
            this.account3.TabIndex = 16;
            this.account3.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
            this.account3.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form_KeyUp);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label9.Location = new System.Drawing.Point(280, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 3;
            this.label9.Text = "密码：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label10.Location = new System.Drawing.Point(11, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 2;
            this.label10.Text = "帐号：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.copyacc1);
            this.groupBox1.Controls.Add(this.showPBox1);
            this.groupBox1.Controls.Add(this.copypwd1);
            this.groupBox1.Controls.Add(this.password1);
            this.groupBox1.Controls.Add(this.account1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.ForeColor = System.Drawing.Color.Teal;
            this.groupBox1.Location = new System.Drawing.Point(15, 70);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(560, 41);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "账户1";
            // 
            // copyacc1
            // 
            this.copyacc1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.copyacc1.Location = new System.Drawing.Point(249, 12);
            this.copyacc1.Name = "copyacc1";
            this.copyacc1.Size = new System.Drawing.Size(24, 24);
            this.copyacc1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.copyacc1.TabIndex = 27;
            this.copyacc1.TabStop = false;
            this.copyacc1.Tag = "copy";
            this.toolTip1.SetToolTip(this.copyacc1, "点击复制账号1");
            this.copyacc1.Click += new System.EventHandler(this.button_func);
            this.copyacc1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.copyacc1.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.copyacc1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.copyacc1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // showPBox1
            // 
            this.showPBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.showPBox1.Location = new System.Drawing.Point(530, 12);
            this.showPBox1.Name = "showPBox1";
            this.showPBox1.Size = new System.Drawing.Size(24, 24);
            this.showPBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.showPBox1.TabIndex = 15;
            this.showPBox1.TabStop = false;
            this.showPBox1.Tag = "lamp";
            this.toolTip1.SetToolTip(this.showPBox1, "显示密码1为明文");
            this.showPBox1.Click += new System.EventHandler(this.button_func);
            this.showPBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.showPBox1.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.showPBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.showPBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // copypwd1
            // 
            this.copypwd1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.copypwd1.Location = new System.Drawing.Point(498, 12);
            this.copypwd1.Name = "copypwd1";
            this.copypwd1.Size = new System.Drawing.Size(24, 24);
            this.copypwd1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.copypwd1.TabIndex = 14;
            this.copypwd1.TabStop = false;
            this.copypwd1.Tag = "copy";
            this.toolTip1.SetToolTip(this.copypwd1, "点击复制密码1");
            this.copypwd1.Click += new System.EventHandler(this.button_func);
            this.copypwd1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.copypwd1.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.copypwd1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.copypwd1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // password1
            // 
            this.password1.Location = new System.Drawing.Point(314, 14);
            this.password1.Name = "password1";
            this.password1.PasswordChar = '◆';
            this.password1.Size = new System.Drawing.Size(177, 21);
            this.password1.TabIndex = 13;
            this.password1.Tag = "";
            this.password1.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
            this.password1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form_KeyUp);
            // 
            // account1
            // 
            this.account1.Location = new System.Drawing.Point(45, 14);
            this.account1.Name = "account1";
            this.account1.Size = new System.Drawing.Size(197, 21);
            this.account1.TabIndex = 12;
            this.account1.TextChanged += new System.EventHandler(this.TextBoxTextChanged);
            this.account1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form_KeyUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(280, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "密码：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(11, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "帐号：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "地 址：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "名 称：";
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.info_label);
            this.panel4.Location = new System.Drawing.Point(335, 189);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(230, 64);
            this.panel4.TabIndex = 25;
            this.panel4.Visible = false;
            // 
            // info_label
            // 
            this.info_label.AutoSize = true;
            this.info_label.BackColor = System.Drawing.Color.Transparent;
            this.info_label.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.info_label.ForeColor = System.Drawing.Color.White;
            this.info_label.Location = new System.Drawing.Point(66, 19);
            this.info_label.Name = "info_label";
            this.info_label.Size = new System.Drawing.Size(106, 24);
            this.info_label.TabIndex = 6;
            this.info_label.Text = "保存成功";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.icon_contextMenu;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseClick);
            // 
            // icon_contextMenu
            // 
            this.icon_contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.显示窗口ToolStripMenuItem,
            this.锁定ToolStripMenuItem1,
            this.退出ToolStripMenuItem2});
            this.icon_contextMenu.Name = "icon_contextMenu";
            this.icon_contextMenu.Size = new System.Drawing.Size(125, 70);
            // 
            // 显示窗口ToolStripMenuItem
            // 
            this.显示窗口ToolStripMenuItem.Name = "显示窗口ToolStripMenuItem";
            this.显示窗口ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.显示窗口ToolStripMenuItem.Text = "隐藏窗口";
            this.显示窗口ToolStripMenuItem.Click += new System.EventHandler(this.显示窗口ToolStripMenuItem_Click);
            // 
            // 锁定ToolStripMenuItem1
            // 
            this.锁定ToolStripMenuItem1.Name = "锁定ToolStripMenuItem1";
            this.锁定ToolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.锁定ToolStripMenuItem1.Text = "锁定";
            this.锁定ToolStripMenuItem1.Click += new System.EventHandler(this.锁定保险箱ToolStripMenuItem_Click);
            // 
            // 退出ToolStripMenuItem2
            // 
            this.退出ToolStripMenuItem2.Name = "退出ToolStripMenuItem2";
            this.退出ToolStripMenuItem2.Size = new System.Drawing.Size(124, 22);
            this.退出ToolStripMenuItem2.Text = "退出";
            this.退出ToolStripMenuItem2.Click += new System.EventHandler(this.退出ToolStripMenuItem1_Click);
            // 
            // close_box
            // 
            this.close_box.BackColor = System.Drawing.Color.Transparent;
            this.close_box.Cursor = System.Windows.Forms.Cursors.Hand;
            this.close_box.Location = new System.Drawing.Point(873, 4);
            this.close_box.Name = "close_box";
            this.close_box.Size = new System.Drawing.Size(21, 21);
            this.close_box.TabIndex = 26;
            this.close_box.TabStop = false;
            this.close_box.Tag = "close";
            this.close_box.Click += new System.EventHandler(this.close_box_Click);
            this.close_box.MouseDown += new System.Windows.Forms.MouseEventHandler(this.box_mouse_down);
            this.close_box.MouseEnter += new System.EventHandler(this.box_mouse_enter);
            this.close_box.MouseLeave += new System.EventHandler(this.box_mouse_leave);
            this.close_box.MouseUp += new System.Windows.Forms.MouseEventHandler(this.box_mouse_up);
            // 
            // min_box
            // 
            this.min_box.BackColor = System.Drawing.Color.Transparent;
            this.min_box.Cursor = System.Windows.Forms.Cursors.Hand;
            this.min_box.Location = new System.Drawing.Point(852, 4);
            this.min_box.Name = "min_box";
            this.min_box.Size = new System.Drawing.Size(21, 21);
            this.min_box.TabIndex = 27;
            this.min_box.TabStop = false;
            this.min_box.Tag = "min";
            this.min_box.Click += new System.EventHandler(this.min_box_Click);
            this.min_box.MouseDown += new System.Windows.Forms.MouseEventHandler(this.box_mouse_down);
            this.min_box.MouseEnter += new System.EventHandler(this.box_mouse_enter);
            this.min_box.MouseLeave += new System.EventHandler(this.box_mouse_leave);
            this.min_box.MouseUp += new System.Windows.Forms.MouseEventHandler(this.box_mouse_up);
            // 
            // menu_box
            // 
            this.menu_box.BackColor = System.Drawing.Color.Transparent;
            this.menu_box.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menu_box.Location = new System.Drawing.Point(831, 4);
            this.menu_box.Name = "menu_box";
            this.menu_box.Size = new System.Drawing.Size(21, 21);
            this.menu_box.TabIndex = 28;
            this.menu_box.TabStop = false;
            this.menu_box.Tag = "menu";
            this.menu_box.Click += new System.EventHandler(this.menu_box_Click);
            this.menu_box.MouseDown += new System.Windows.Forms.MouseEventHandler(this.box_mouse_down);
            this.menu_box.MouseEnter += new System.EventHandler(this.box_mouse_enter);
            this.menu_box.MouseLeave += new System.EventHandler(this.box_mouse_leave);
            this.menu_box.MouseUp += new System.Windows.Forms.MouseEventHandler(this.box_mouse_up);
            // 
            // skin_box
            // 
            this.skin_box.BackColor = System.Drawing.Color.Transparent;
            this.skin_box.Cursor = System.Windows.Forms.Cursors.Hand;
            this.skin_box.Location = new System.Drawing.Point(810, 4);
            this.skin_box.Name = "skin_box";
            this.skin_box.Size = new System.Drawing.Size(21, 21);
            this.skin_box.TabIndex = 29;
            this.skin_box.TabStop = false;
            this.skin_box.Tag = "skin";
            this.skin_box.Click += new System.EventHandler(this.skin_box_Click);
            this.skin_box.MouseDown += new System.Windows.Forms.MouseEventHandler(this.box_mouse_down);
            this.skin_box.MouseEnter += new System.EventHandler(this.box_mouse_enter);
            this.skin_box.MouseLeave += new System.EventHandler(this.box_mouse_leave);
            this.skin_box.MouseUp += new System.Windows.Forms.MouseEventHandler(this.box_mouse_up);
            // 
            // feedback_box
            // 
            this.feedback_box.BackColor = System.Drawing.Color.Transparent;
            this.feedback_box.Cursor = System.Windows.Forms.Cursors.Hand;
            this.feedback_box.Location = new System.Drawing.Point(789, 4);
            this.feedback_box.Name = "feedback_box";
            this.feedback_box.Size = new System.Drawing.Size(21, 21);
            this.feedback_box.TabIndex = 30;
            this.feedback_box.TabStop = false;
            this.feedback_box.Tag = "feedback";
            this.feedback_box.Click += new System.EventHandler(this.feedback_box_Click);
            this.feedback_box.MouseDown += new System.Windows.Forms.MouseEventHandler(this.box_mouse_down);
            this.feedback_box.MouseEnter += new System.EventHandler(this.box_mouse_enter);
            this.feedback_box.MouseLeave += new System.EventHandler(this.box_mouse_leave);
            this.feedback_box.MouseUp += new System.Windows.Forms.MouseEventHandler(this.box_mouse_up);
            // 
            // logo
            // 
            this.logo.BackColor = System.Drawing.Color.Transparent;
            this.logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.logo.Location = new System.Drawing.Point(8, 3);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(50, 50);
            this.logo.TabIndex = 31;
            this.logo.TabStop = false;
            // 
            // title1_pox
            // 
            this.title1_pox.Location = new System.Drawing.Point(64, 10);
            this.title1_pox.Name = "title1_pox";
            this.title1_pox.Size = new System.Drawing.Size(243, 36);
            this.title1_pox.TabIndex = 32;
            this.title1_pox.TabStop = false;
            // 
            // contextMenu
            // 
            this.contextMenu.BackColor = System.Drawing.Color.White;
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.锁定保险箱ToolStripMenuItem,
            this.开机启动ToolStripMenuItem,
            this.修改密码ToolStripMenuItem1,
            this.帮助ToolStripMenuItem1,
            this.toolStripSeparator7,
            this.保存数据ToolStripMenuItem,
            this.备份数据ToolStripMenuItem,
            this.还原数据ToolStripMenuItem,
            this.工具ToolStripMenuItem,
            this.toolStripSeparator8,
            this.退出ToolStripMenuItem1});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.contextMenu.Size = new System.Drawing.Size(180, 214);
            // 
            // 锁定保险箱ToolStripMenuItem
            // 
            this.锁定保险箱ToolStripMenuItem.Name = "锁定保险箱ToolStripMenuItem";
            this.锁定保险箱ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.锁定保险箱ToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.锁定保险箱ToolStripMenuItem.Text = "锁定保险箱";
            this.锁定保险箱ToolStripMenuItem.Click += new System.EventHandler(this.锁定保险箱ToolStripMenuItem_Click);
            // 
            // 开机启动ToolStripMenuItem
            // 
            this.开机启动ToolStripMenuItem.Name = "开机启动ToolStripMenuItem";
            this.开机启动ToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.开机启动ToolStripMenuItem.Text = "开机启动";
            this.开机启动ToolStripMenuItem.Click += new System.EventHandler(this.开机启动ToolStripMenuItem_Click);
            // 
            // 修改密码ToolStripMenuItem1
            // 
            this.修改密码ToolStripMenuItem1.Name = "修改密码ToolStripMenuItem1";
            this.修改密码ToolStripMenuItem1.Size = new System.Drawing.Size(179, 22);
            this.修改密码ToolStripMenuItem1.Text = "修改密码";
            this.修改密码ToolStripMenuItem1.Click += new System.EventHandler(this.修改密码ToolStripMenuItem1_Click);
            // 
            // 帮助ToolStripMenuItem1
            // 
            this.帮助ToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.关于ToolStripMenuItem2,
            this.使用说明ToolStripMenuItem,
            this.官方论坛ToolStripMenuItem});
            this.帮助ToolStripMenuItem1.Name = "帮助ToolStripMenuItem1";
            this.帮助ToolStripMenuItem1.Size = new System.Drawing.Size(179, 22);
            this.帮助ToolStripMenuItem1.Text = "帮助";
            // 
            // 关于ToolStripMenuItem2
            // 
            this.关于ToolStripMenuItem2.Name = "关于ToolStripMenuItem2";
            this.关于ToolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.关于ToolStripMenuItem2.Text = "关于";
            this.关于ToolStripMenuItem2.Click += new System.EventHandler(this.关于ToolStripMenuItem2_Click);
            // 
            // 使用说明ToolStripMenuItem
            // 
            this.使用说明ToolStripMenuItem.Name = "使用说明ToolStripMenuItem";
            this.使用说明ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.使用说明ToolStripMenuItem.Text = "说明";
            this.使用说明ToolStripMenuItem.Visible = false;
            this.使用说明ToolStripMenuItem.Click += new System.EventHandler(this.使用说明ToolStripMenuItem_Click);
            // 
            // 官方论坛ToolStripMenuItem
            // 
            this.官方论坛ToolStripMenuItem.Name = "官方论坛ToolStripMenuItem";
            this.官方论坛ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.官方论坛ToolStripMenuItem.Text = "官方论坛";
            this.官方论坛ToolStripMenuItem.Click += new System.EventHandler(this.官网ToolStripMenuItem1_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(176, 6);
            // 
            // 保存数据ToolStripMenuItem
            // 
            this.保存数据ToolStripMenuItem.Name = "保存数据ToolStripMenuItem";
            this.保存数据ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.保存数据ToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.保存数据ToolStripMenuItem.Text = "保存数据";
            this.保存数据ToolStripMenuItem.Click += new System.EventHandler(this.保存数据ToolStripMenuItem_Click);
            // 
            // 备份数据ToolStripMenuItem
            // 
            this.备份数据ToolStripMenuItem.Name = "备份数据ToolStripMenuItem";
            this.备份数据ToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.备份数据ToolStripMenuItem.Text = "备份数据";
            this.备份数据ToolStripMenuItem.Click += new System.EventHandler(this.导出数据ToolStripMenuItem_Click);
            // 
            // 还原数据ToolStripMenuItem
            // 
            this.还原数据ToolStripMenuItem.Name = "还原数据ToolStripMenuItem";
            this.还原数据ToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.还原数据ToolStripMenuItem.Text = "还原数据";
            this.还原数据ToolStripMenuItem.Click += new System.EventHandler(this.导入数据ToolStripMenuItem_Click);
            // 
            // 工具ToolStripMenuItem
            // 
            this.工具ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.加密文件ToolStripMenuItem,
            this.文件解密ToolStripMenuItem});
            this.工具ToolStripMenuItem.Name = "工具ToolStripMenuItem";
            this.工具ToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.工具ToolStripMenuItem.Text = "工具";
            // 
            // 加密文件ToolStripMenuItem
            // 
            this.加密文件ToolStripMenuItem.Name = "加密文件ToolStripMenuItem";
            this.加密文件ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.加密文件ToolStripMenuItem.Text = "文件加密";
            this.加密文件ToolStripMenuItem.Click += new System.EventHandler(this.加密文件ToolStripMenuItem_Click);
            // 
            // 文件解密ToolStripMenuItem
            // 
            this.文件解密ToolStripMenuItem.Name = "文件解密ToolStripMenuItem";
            this.文件解密ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.文件解密ToolStripMenuItem.Text = "文件解密";
            this.文件解密ToolStripMenuItem.Click += new System.EventHandler(this.文件解密ToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(176, 6);
            // 
            // 退出ToolStripMenuItem1
            // 
            this.退出ToolStripMenuItem1.Name = "退出ToolStripMenuItem1";
            this.退出ToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.退出ToolStripMenuItem1.Size = new System.Drawing.Size(179, 22);
            this.退出ToolStripMenuItem1.Text = "退出";
            this.退出ToolStripMenuItem1.Click += new System.EventHandler(this.退出ToolStripMenuItem1_Click);
            // 
            // title2_box
            // 
            this.title2_box.Location = new System.Drawing.Point(638, 31);
            this.title2_box.Name = "title2_box";
            this.title2_box.Size = new System.Drawing.Size(256, 18);
            this.title2_box.TabIndex = 34;
            this.title2_box.TabStop = false;
            // 
            // panel_skin
            // 
            this.panel_skin.Controls.Add(this.choosebox_silver);
            this.panel_skin.Controls.Add(this.choosebox_blank);
            this.panel_skin.Controls.Add(this.choosebox_red);
            this.panel_skin.Controls.Add(this.choosebox_blue);
            this.panel_skin.Controls.Add(this.choosebox_green);
            this.panel_skin.Controls.Add(this.boxskin_silver);
            this.panel_skin.Controls.Add(this.boxskin_blank);
            this.panel_skin.Controls.Add(this.boxskin_red);
            this.panel_skin.Controls.Add(this.boxskin_blue);
            this.panel_skin.Controls.Add(this.boxskin_green);
            this.panel_skin.Location = new System.Drawing.Point(526, 26);
            this.panel_skin.Name = "panel_skin";
            this.panel_skin.Size = new System.Drawing.Size(306, 56);
            this.panel_skin.TabIndex = 35;
            this.panel_skin.Visible = false;
            this.panel_skin.MouseEnter += new System.EventHandler(this.panel_skin_MouseEnter);
            // 
            // choosebox_silver
            // 
            this.choosebox_silver.Location = new System.Drawing.Point(281, 32);
            this.choosebox_silver.Name = "choosebox_silver";
            this.choosebox_silver.Size = new System.Drawing.Size(16, 16);
            this.choosebox_silver.TabIndex = 9;
            this.choosebox_silver.TabStop = false;
            this.choosebox_silver.Tag = "skinbox_silver";
            this.choosebox_silver.Visible = false;
            // 
            // choosebox_blank
            // 
            this.choosebox_blank.Location = new System.Drawing.Point(221, 32);
            this.choosebox_blank.Name = "choosebox_blank";
            this.choosebox_blank.Size = new System.Drawing.Size(16, 16);
            this.choosebox_blank.TabIndex = 8;
            this.choosebox_blank.TabStop = false;
            this.choosebox_blank.Tag = "skinbox_blank";
            this.choosebox_blank.Visible = false;
            // 
            // choosebox_red
            // 
            this.choosebox_red.Location = new System.Drawing.Point(161, 32);
            this.choosebox_red.Name = "choosebox_red";
            this.choosebox_red.Size = new System.Drawing.Size(16, 16);
            this.choosebox_red.TabIndex = 7;
            this.choosebox_red.TabStop = false;
            this.choosebox_red.Tag = "skinbox_red";
            this.choosebox_red.Visible = false;
            // 
            // choosebox_blue
            // 
            this.choosebox_blue.Location = new System.Drawing.Point(101, 32);
            this.choosebox_blue.Name = "choosebox_blue";
            this.choosebox_blue.Size = new System.Drawing.Size(16, 16);
            this.choosebox_blue.TabIndex = 6;
            this.choosebox_blue.TabStop = false;
            this.choosebox_blue.Tag = "skinbox_blue";
            this.choosebox_blue.Visible = false;
            // 
            // choosebox_green
            // 
            this.choosebox_green.Location = new System.Drawing.Point(41, 32);
            this.choosebox_green.Name = "choosebox_green";
            this.choosebox_green.Size = new System.Drawing.Size(16, 16);
            this.choosebox_green.TabIndex = 5;
            this.choosebox_green.TabStop = false;
            this.choosebox_green.Tag = "skinbox_green";
            this.choosebox_green.Visible = false;
            // 
            // boxskin_silver
            // 
            this.boxskin_silver.Location = new System.Drawing.Point(246, 5);
            this.boxskin_silver.Name = "boxskin_silver";
            this.boxskin_silver.Size = new System.Drawing.Size(54, 46);
            this.boxskin_silver.TabIndex = 4;
            this.boxskin_silver.TabStop = false;
            this.boxskin_silver.Tag = "skinbox_silver";
            this.boxskin_silver.Click += new System.EventHandler(this.boxskin_Click);
            this.boxskin_silver.MouseEnter += new System.EventHandler(this.skin_box_MouseEnter);
            this.boxskin_silver.MouseLeave += new System.EventHandler(this.skin_box_MouseLeave);
            // 
            // boxskin_blank
            // 
            this.boxskin_blank.Location = new System.Drawing.Point(186, 5);
            this.boxskin_blank.Name = "boxskin_blank";
            this.boxskin_blank.Size = new System.Drawing.Size(54, 46);
            this.boxskin_blank.TabIndex = 3;
            this.boxskin_blank.TabStop = false;
            this.boxskin_blank.Tag = "skinbox_blank";
            this.boxskin_blank.Click += new System.EventHandler(this.boxskin_Click);
            this.boxskin_blank.MouseEnter += new System.EventHandler(this.skin_box_MouseEnter);
            this.boxskin_blank.MouseLeave += new System.EventHandler(this.skin_box_MouseLeave);
            // 
            // boxskin_red
            // 
            this.boxskin_red.Location = new System.Drawing.Point(126, 5);
            this.boxskin_red.Name = "boxskin_red";
            this.boxskin_red.Size = new System.Drawing.Size(54, 46);
            this.boxskin_red.TabIndex = 2;
            this.boxskin_red.TabStop = false;
            this.boxskin_red.Tag = "skinbox_red";
            this.boxskin_red.Click += new System.EventHandler(this.boxskin_Click);
            this.boxskin_red.MouseEnter += new System.EventHandler(this.skin_box_MouseEnter);
            this.boxskin_red.MouseLeave += new System.EventHandler(this.skin_box_MouseLeave);
            // 
            // boxskin_blue
            // 
            this.boxskin_blue.Location = new System.Drawing.Point(66, 5);
            this.boxskin_blue.Name = "boxskin_blue";
            this.boxskin_blue.Size = new System.Drawing.Size(54, 46);
            this.boxskin_blue.TabIndex = 1;
            this.boxskin_blue.TabStop = false;
            this.boxskin_blue.Tag = "skinbox_blue";
            this.boxskin_blue.Click += new System.EventHandler(this.boxskin_Click);
            this.boxskin_blue.MouseEnter += new System.EventHandler(this.skin_box_MouseEnter);
            this.boxskin_blue.MouseLeave += new System.EventHandler(this.skin_box_MouseLeave);
            // 
            // boxskin_green
            // 
            this.boxskin_green.Location = new System.Drawing.Point(6, 5);
            this.boxskin_green.Name = "boxskin_green";
            this.boxskin_green.Size = new System.Drawing.Size(54, 46);
            this.boxskin_green.TabIndex = 0;
            this.boxskin_green.TabStop = false;
            this.boxskin_green.Tag = "skinbox_green";
            this.boxskin_green.Click += new System.EventHandler(this.boxskin_Click);
            this.boxskin_green.MouseEnter += new System.EventHandler(this.skin_box_MouseEnter);
            this.boxskin_green.MouseLeave += new System.EventHandler(this.skin_box_MouseLeave);
            // 
            // version
            // 
            this.version.AutoSize = true;
            this.version.BackColor = System.Drawing.Color.Transparent;
            this.version.ForeColor = System.Drawing.Color.White;
            this.version.Location = new System.Drawing.Point(313, 33);
            this.version.Name = "version";
            this.version.Size = new System.Drawing.Size(0, 12);
            this.version.TabIndex = 36;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.ForestGreen;
            this.ClientSize = new System.Drawing.Size(900, 600);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel_skin);
            this.Controls.Add(this.version);
            this.Controls.Add(this.title2_box);
            this.Controls.Add(this.title1_pox);
            this.Controls.Add(this.logo);
            this.Controls.Add(this.feedback_box);
            this.Controls.Add(this.skin_box);
            this.Controls.Add(this.menu_box);
            this.Controls.Add(this.min_box);
            this.Controls.Add(this.close_box);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(900, 600);
            this.MinimumSize = new System.Drawing.Size(900, 600);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "密码安全保险箱";
            this.TransparencyKey = System.Drawing.Color.LightSkyBlue;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tree_contextMenu.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rich_back)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.goto_box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sync_box)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copyacc5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showPBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.copypwd5)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copyacc4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showPBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.copypwd4)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copyacc2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showPBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.copypwd2)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copyacc3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showPBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.copypwd3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.copyacc1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showPBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.copypwd1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.icon_contextMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.close_box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.min_box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.menu_box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.skin_box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.feedback_box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.title1_pox)).EndInit();
            this.contextMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.title2_box)).EndInit();
            this.panel_skin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.choosebox_silver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.choosebox_blank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.choosebox_red)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.choosebox_blue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.choosebox_green)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxskin_silver)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxskin_blank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxskin_red)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxskin_blue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.boxskin_green)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.ToolStripMenuItem 操作ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 添加分类ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 保存ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 关于ToolStripMenuItem;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox_name;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TextBox textBox_adress;
        private System.Windows.Forms.TextBox password5;
        private System.Windows.Forms.TextBox account5;
        private System.Windows.Forms.TextBox password4;
        private System.Windows.Forms.TextBox account4;
        private System.Windows.Forms.TextBox password2;
        private System.Windows.Forms.TextBox account2;
        private System.Windows.Forms.TextBox password3;
        private System.Windows.Forms.TextBox account3;
        private System.Windows.Forms.TextBox password1;
        private System.Windows.Forms.TextBox account1;
        private System.Windows.Forms.PictureBox copypwd1;
        private System.Windows.Forms.PictureBox showPBox1;
        private System.Windows.Forms.PictureBox showPBox5;
        private System.Windows.Forms.PictureBox copypwd5;
        private System.Windows.Forms.PictureBox showPBox4;
        private System.Windows.Forms.PictureBox copypwd4;
        private System.Windows.Forms.PictureBox showPBox2;
        private System.Windows.Forms.PictureBox copypwd2;
        private System.Windows.Forms.PictureBox showPBox3;
        private System.Windows.Forms.PictureBox copypwd3;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem 锁定ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 选项ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 关于ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 官网ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem treeMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 添加分类ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem 添加子类ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 修改名称ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem 上移ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 下移ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem 获取图标ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 更改图标ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 还原默认图标ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip tree_contextMenu;
        private System.Windows.Forms.ToolStripMenuItem menu_addGenre;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem menu_add_son_genre;
        private System.Windows.Forms.ToolStripMenuItem menu_update_item;
        private System.Windows.Forms.ToolStripMenuItem menu_del_item;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem menu_up_move;
        private System.Windows.Forms.ToolStripMenuItem menu_down_move;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem menu_get_icon;
        private System.Windows.Forms.ToolStripMenuItem menu_set_local;
        private System.Windows.Forms.ToolStripMenuItem menu_reset_icon;
        private System.Windows.Forms.PictureBox copyacc5;
        private System.Windows.Forms.PictureBox copyacc4;
        private System.Windows.Forms.PictureBox copyacc2;
        private System.Windows.Forms.PictureBox copyacc3;
        private System.Windows.Forms.PictureBox copyacc1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox sync_box;
        private System.Windows.Forms.PictureBox goto_box;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label info_label;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.PictureBox close_box;
        private System.Windows.Forms.PictureBox min_box;
        private System.Windows.Forms.PictureBox menu_box;
        private System.Windows.Forms.PictureBox skin_box;
        private System.Windows.Forms.PictureBox feedback_box;
        private System.Windows.Forms.PictureBox rich_back;
        private System.Windows.Forms.PictureBox logo;
        private System.Windows.Forms.PictureBox title1_pox;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem 锁定保险箱ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 开机启动ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 关于ToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem 使用说明ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 官方论坛ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 备份数据ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 还原数据ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem 展开全部ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 收缩全部ToolStripMenuItem;
        private System.Windows.Forms.PictureBox title2_box;
        private System.Windows.Forms.ToolStripMenuItem 工具ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 加密文件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 文件解密ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 修改密码ToolStripMenuItem1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Panel panel_skin;
        private System.Windows.Forms.PictureBox boxskin_green;
        private System.Windows.Forms.PictureBox boxskin_silver;
        private System.Windows.Forms.PictureBox boxskin_blank;
        private System.Windows.Forms.PictureBox boxskin_red;
        private System.Windows.Forms.PictureBox boxskin_blue;
        private System.Windows.Forms.PictureBox choosebox_green;
        private System.Windows.Forms.PictureBox choosebox_silver;
        private System.Windows.Forms.PictureBox choosebox_blank;
        private System.Windows.Forms.PictureBox choosebox_red;
        private System.Windows.Forms.PictureBox choosebox_blue;
        private System.Windows.Forms.TextBox searchKey;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label version;
        private System.Windows.Forms.ContextMenuStrip icon_contextMenu;
        private System.Windows.Forms.ToolStripMenuItem 显示窗口ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 锁定ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem 保存数据ToolStripMenuItem;
    }
}

