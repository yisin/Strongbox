﻿using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web;
using System.Net;
using System.Runtime.InteropServices;
using YSTools;

namespace Strongbox
{
    public partial class Form1 : Form
    {
        public static string appDir = Application.StartupPath + "/";

        public static String appName = "Strongbox";
        public static double appVersion = 1.1d;

        Utils u = new Utils();

        public static bool islock = false;
        public int[] pwdShow = { 1, 1, 1, 1, 1 };
        public TreeNode currSelectedNode = null;
        public String currNodeId = "";
        public static int treeCount = 0;
        public static bool saveIng = false;

        public static List<TreeObj> treeList = new List<TreeObj>();

        public delegate void MyDelegate(Control trol);
        public delegate void TreeDelegate(int type, String id);
        public delegate void PanelDelegate(int type);
        public delegate void ShowDelegate();

        public static LockScreen ls = new LockScreen();

        public Form1()
        {
            InitializeComponent();

            try
            {
                showPanelAnimote(2);

                //SetStyle(ControlStyles.UserPaint, true);
                SetStyle(ControlStyles.AllPaintingInWmPaint, true); // 禁止擦除背景.
                SetStyle(ControlStyles.DoubleBuffer, true); //双缓冲

                YSTools.YSFile.CreateHiddenDir(Utils.baseDir);
                this.notifyIcon1.Text = this.Text;
                ls.Visible = false;

            }
            catch(Exception ex){
                Console.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// 窗口加载完成
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                // 加载数据
                u.loadConfig();

                treeView1.LabelEdit = true;

                timer1.Interval = 50;
                timer1.Tick += timer1_Tick;
                timer1.Start();

                timer2.Interval = 5000;
                timer2.Tick += timer2_Tick;
                timer2.Start();

                //图标显示在托盘区 
                notifyIcon1.Visible = true;

                changeSkin();

                version.Text = "v" + appVersion;
                boxskin_green.Image = u.GetImage("skinbox_green");
                boxskin_blue.Image = u.GetImage("skinbox_blue");
                boxskin_blank.Image = u.GetImage("skinbox_blank");
                boxskin_red.Image = u.GetImage("skinbox_red");
                boxskin_silver.Image = u.GetImage("skinbox_silver");
                choosebox_green.Image = u.GetImage("choose_skin_green");
                choosebox_blue.Image = u.GetImage("choose_skin_blue");
                choosebox_blank.Image = u.GetImage("choose_skin_blank");
                choosebox_silver.Image = u.GetImage("choose_skin_silver");
                choosebox_red.Image = u.GetImage("choose_skin_red");

                for (int i = 0; i < Utils.appImageList.Count; i++)
                {
                    if (File.Exists(Utils.appImageList[i]))
                    {
                        imageList1.Images.Add(Image.FromFile(Utils.appImageList[i]));
                    }
                }
                if (imageList1.Images.Count == 0)
                {
                    imageList1.Images.Add(Image.FromFile(Form1.appDir + "resource/images/net_32.ico"));
                    imageList1.Images.Add(Image.FromFile(Form1.appDir + "resource/images/yun32.ico"));
                    imageList1.Images.Add(Image.FromFile(Form1.appDir + "resource/images/web.ico"));
                }

                // 初始化树数据
                initTree("_", 0);

                // 加锁
                u.LockStrongBox(false);
            }
            catch
            {
            }            
        }

        void timer2_Tick(object sender, EventArgs e)
        {
            if (!islock)
            {
                long times1 = Utils.lastOparateTime;
                long times2 = DateTime.Now.Ticks;
                long times3 = (times2 - times1) / (10000 * 1000);
                //Console.WriteLine("{0} - {1} = {2}", times2, times1, times3);
                if (times3 > 300)
                {
                    // 加锁
                    u.LockStrongBox(true);
                }
            }
            else
            {
                Utils.SetLastOparateTime();                
            }            
        }

        public void timer1_Tick(object sender, EventArgs e)
        {
            if (currSelectedNode != null)
            {
                if (!currNodeId.Equals(currSelectedNode.Name))
                {
                    currNodeId = currSelectedNode.Name;
                    if (currNodeId.IndexOf("_") == -1 || currSelectedNode.Nodes.Count > 0)
                    {
                        ThreadPool.QueueUserWorkItem((a) =>
                        {
                            PanelDelegate pd = new PanelDelegate(showPanelAnimote);
                            Invoke(pd, 2);
                        });
                    }
                    else
                    {
                        ThreadPool.QueueUserWorkItem((a) =>
                        {
                            PanelDelegate pd = new PanelDelegate(showPanelAnimote);
                            Invoke(pd, 1);
                        });
                    }
                    TreeObj tree = getTreeObj(currNodeId);
                    if (tree != null)
                    {
                        textBox_name.Text = tree.name;
                        textBox_adress.Text = tree.address;
                        account1.Text = tree.account1;
                        account2.Text = tree.account2;
                        account3.Text = tree.account3;
                        account4.Text = tree.account4;
                        account5.Text = tree.account5;
                        password1.Text = tree.password1;
                        password2.Text = tree.password2;
                        password3.Text = tree.password3;
                        password4.Text = tree.password4;
                        password5.Text = tree.password5;
                        richTextBox1.Text = tree.remark;
                    }
                }
            }
            else
            {
                currNodeId = "";
                //----==
                showPanelAnimote(2);
            }

            int tnum = treeView1.Nodes.Count;
            searchKey.Visible = tnum > 0;
            label11.Visible = tnum > 0;
            label12.Visible = tnum > 0;
            label15.Visible = tnum == 0;
        }

        /// <summary>
        /// 窗口关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            Thread.Sleep(200);
            ThreadPool.QueueUserWorkItem((a) => {
               u.ExitApplication();
            });           
        }

        public void firstLockStrongbox()
        {
            LockScreen.initPwd = true;
            //显示窗口ToolStripMenuItem.Text = "取消锁定保险箱";
            显示窗口ToolStripMenuItem.Visible = false;
            ls.ShowDialog();
        }


        public void changeSkin()
        {
            this.BackgroundImage = u.GetImageBySkin("pix");
            logo.BackgroundImage = u.GetImage("box");
            title1_pox.BackgroundImage = u.GetImageBySkin("title");
            title2_box.BackgroundImage = u.GetImageBySkin("title_min");
            close_box.Image = u.GetImageBySkin("close");
            min_box.Image = u.GetImageBySkin("min");
            skin_box.Image = u.GetImageBySkin("skin");
            menu_box.Image = u.GetImageBySkin("menu");
            feedback_box.Image = u.GetImageBySkin("feedback");
            sync_box.Image = u.GetImage("sync");
            goto_box.Image = u.GetImage("gonet");
            panel_skin.BackgroundImage = u.GetImageBySkin("panel_skin");
            version.ForeColor = Color.FromArgb(255, 250, 250, 250);
            if(Utils.skin.Equals("green")){
                Color col = Color.FromArgb(255, 247, 247, 247);
                panel1.BackColor = col;
                panel2.BackColor = col;
                label11.BackColor = col;
                label12.BackColor = col;
                label15.BackColor = col;
                treeView1.BackColor = col;
            }
            else if (Utils.skin.Equals("blue"))
            {
                Color col = Color.FromArgb(255, 247, 247, 247);
                panel1.BackColor = col;
                panel2.BackColor = col;
                label11.BackColor = col;
                label12.BackColor = col;
                label15.BackColor = col;
                treeView1.BackColor = col;
            }
            else if (Utils.skin.Equals("red"))
            {
                Color col = Color.FromArgb(255, 247, 247, 247);
                panel1.BackColor = col;
                panel2.BackColor = col;
                label11.BackColor = col;
                label12.BackColor = col;
                label15.BackColor = col;
                treeView1.BackColor = col;
            }
            else if (Utils.skin.Equals("blank"))
            {
                Color col = Color.FromArgb(255, 247, 247, 247);
                panel1.BackColor = col;
                panel2.BackColor = col;
                label11.BackColor = col;
                label12.BackColor = col;
                label15.BackColor = col;
                treeView1.BackColor = col;
            }
            else if (Utils.skin.Equals("silver"))
            {
                Color col = Color.FromArgb(255, 247, 247, 247);
                panel1.BackColor = col;
                panel2.BackColor = col;
                label11.BackColor = col;
                label12.BackColor = col;
                label15.BackColor = col;
                treeView1.BackColor = col;

                version.ForeColor = Color.FromArgb(255, 80, 80, 80);
            }
            ls.changeSkin();


            copyacc1.Image = u.GetImage("copy");
            copyacc2.Image = u.GetImage("copy");
            copyacc3.Image = u.GetImage("copy");
            copyacc4.Image = u.GetImage("copy");
            copyacc5.Image = u.GetImage("copy");

            copypwd1.Image = u.GetImage("copy");
            copypwd2.Image = u.GetImage("copy");
            copypwd3.Image = u.GetImage("copy");
            copypwd4.Image = u.GetImage("copy");
            copypwd5.Image = u.GetImage("copy");

            showPBox1.Image = u.GetImage("lamp");
            showPBox2.Image = u.GetImage("lamp");
            showPBox3.Image = u.GetImage("lamp");
            showPBox4.Image = u.GetImage("lamp");
            showPBox5.Image = u.GetImage("lamp");

        }

        /// <summary>
        /// 当前选中项：TreeView.SelectedNode 
        /// 增加顶级节点：TreeView.Nodes.Add("Key", "Text") 
        /// 增加同级节点：TreeView.SelectedNode.Parent.Nodes.Add("Key", "Text") 
        /// 增加子节点：TreeView.SelectedNode.Nodes.Add("Key", "Text") 
        /// 全部展开：TreeView.ExpandAll() 
        /// 全部收拢：TreeView.CollapseAll() 
        /// 重新设置TreeView 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 添加分类ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int count = treeView1.Nodes.Count;
            String id = String.Format("{0}", treeCount);
            addTreeNode(id, "新建分类" + treeCount, "_");
            treeCount++;
        }

        private void treeView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (treeView1.Nodes.Count == 0)
            {
                treeView1.SelectedNode.BeginEdit();
            }
        }

        private void treeView1_MouseUp(object sender, MouseEventArgs e)
        {
            TreeNode node = treeView1.GetNodeAt(e.X, e.Y);
            if (node != null)
            {
                if (e.Button == MouseButtons.Right)
                {
                    currSelectedNode = node;
                    treeView1.SelectedNode = currSelectedNode;                    
                }
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                currSelectedNode = treeView1.SelectedNode;
                TreeObj tree = getTreeObj(currSelectedNode.Name);
                if (tree != null && !String.IsNullOrEmpty(tree.address))
                {
                    menu_get_icon.Enabled = currSelectedNode.Nodes.Count == 0;
                }
                else
                {
                    menu_get_icon.Enabled = false;
                }
            }
            menu_down_move.Enabled = treeView1.SelectedNode != null;
            menu_up_move.Enabled = treeView1.SelectedNode != null;
            menu_reset_icon.Enabled = treeView1.SelectedNode != null;
            menu_set_local.Enabled = treeView1.SelectedNode != null;
            menu_del_item.Enabled = treeView1.SelectedNode != null;
        }

        /// <summary>
        /// 新增树节点
        /// </summary>
        /// <param name="type"></param>
        public void addTreeNode(String id, String name, String pid)
        {
            if(pid.Equals("_"))
            { // 一级分类                
                treeView1.Nodes.Add(id, name, 0, 0);
                selectedNode(id, 1);

                TreeObj tree = new TreeObj();
                tree.id = id;
                tree.treeText = name;
                tree.imageIndex = 0;
                tree.order = treeView1.SelectedNode.Index + 1;
                tree.parentId = pid;
                treeList.Add(tree);
            }
            else // 子类
            {
                if (treeView1.SelectedNode != null)
                {
                    if (pid.IndexOf("_") != -1)
                    {
                        TreeObj pobj = getTreeObj(pid);
                        pobj.imageIndex = 1;
                        treeView1.SelectedNode.ImageIndex = 1;
                        treeView1.SelectedNode.SelectedImageIndex = 1;
                    }
                    treeView1.SelectedNode.Nodes.Add(id, name, 2, 2);
                    treeView1.SelectedNode.Expand();
                    selectedNode(id, 1);

                    TreeObj tree = new TreeObj();
                    tree.id = id;
                    tree.treeText = name;
                    tree.imageIndex = 2;
                    tree.order = treeView1.SelectedNode.Index + 1;
                    tree.parentId = pid;
                    treeList.Add(tree);
                }
            }
        }

        public TreeNode selectedNode(String id, int type = 0)
        {
            TreeNode[] treeNodes = treeView1.Nodes.Find(id, true);
            if (treeNodes != null && treeNodes.Length > 0)
            {
                currSelectedNode = treeNodes[0];
                if (treeView1.SelectedNode != null)
                {
                    treeView1.SelectedNode = null;
                }
                treeView1.SelectedNode = currSelectedNode;
                if (currSelectedNode.Name.IndexOf("_") != -1)
                {
                    currSelectedNode.Parent.Expand();
                }
                if (type == 1)
                {
                    currSelectedNode.BeginEdit();
                }
            }
            return currSelectedNode;
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox) sender;
            String tags = pb.Tag.ToString();
            pb.Image = u.GetImage(tags, 1);
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            String tags = pb.Tag.ToString();
            if (tags.Equals("lamp"))
            {
                int index = Convert.ToInt32(pb.Name.Substring(pb.Name.Length - 1));
                if (pwdShow[index - 1] == 2)
                {
                    pb.Image = u.GetImage(tags, 1);
                }
                else
                {
                    pb.Image = u.GetImage(tags, 0);
                }
            }
            else
            {
                pb.Image = u.GetImage(tags, 0);
            }
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left){
                PictureBox pb = (PictureBox)sender;
                String tags = pb.Tag.ToString();
                pb.Image = u.GetImage(tags, 2);
            }            
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                PictureBox pb = (PictureBox)sender;
                String tags = pb.Tag.ToString();                
                if (tags.Equals("lamp"))
                {
                    int index = Convert.ToInt32(pb.Name.Substring(pb.Name.Length - 1));
                    if (pwdShow[index - 1] == 2)
                    {
                        pb.Image = u.GetImage(tags, 1);
                    }
                    else
                    {
                        pb.Image = u.GetImage(tags, 0);
                    }
                }
                else
                {
                    pb.Image = u.GetImage(tags, 0);
                }
            }  
        }

        /// <summary>
        /// 新增子类
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_add_son_genre_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                String pid = treeView1.SelectedNode.Name;
                String id = String.Format("{0}_{1}", pid, treeCount);
                addTreeNode(id, "新建子项" + treeCount, pid);
                treeCount++;
            }
            
        }
        /// <summary>
        /// 修改项目名称
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_update_item_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                treeView1.SelectedNode.BeginEdit();
            }            
        }

        /// <summary>
        /// 删除项目
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_del_item_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                if (MessageBox.Show("您确定要删除此项吗？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    String id = treeView1.SelectedNode.Name;
                    if (id != null && id.IndexOf("_") != -1)
                    {
                        if (treeView1.SelectedNode.Parent.Nodes.Count == 1)
                        {
                            String pid = treeView1.SelectedNode.Parent.Name;
                            if (pid != null && pid.IndexOf("_") != -1)
                            {
                                TreeObj tree = getTreeObj(pid);
                                if (tree != null)
                                {
                                    tree.imageIndex = 2;
                                    treeView1.SelectedNode.Parent.ImageIndex = 2;
                                    treeView1.SelectedNode.Parent.SelectedImageIndex = 2;
                                }                                
                            }
                        }
                    }
                    treeView1.SelectedNode.Remove();
                    // 删除
                    removeTree(id);
                }                
            }  
        }
        /// <summary>
        /// 上移项目
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_up_move_Click(object sender, EventArgs e)
        {
            if(treeView1.SelectedNode != null){                
                int index = treeView1.SelectedNode.Index;
                String id = treeView1.SelectedNode.Name;
                String exchangeId = "";
                if(index != 0){
                    TreeNode node = (TreeNode)treeView1.SelectedNode.Clone();
                    if(id.IndexOf("_") == -1){// id中不包含_，代表顶级分类
                        exchangeId = treeView1.Nodes[index - 1].Name;
                        treeView1.Nodes.Insert(index - 1, node);
                    }
                    else // 子类上移
                    {
                        exchangeId = treeView1.SelectedNode.Parent.Nodes[index - 1].Name;
                        treeView1.SelectedNode.Parent.Nodes.Insert(index - 1, node);
                    }
                    treeView1.SelectedNode.Remove();
                    selectedNode(id);
                    updateTreeOrder(id, exchangeId);
                }
            }
        }
        /// <summary>
        /// 下移项目
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_down_move_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                int index = treeView1.SelectedNode.Index;
                String id = treeView1.SelectedNode.Name;
                String exchangeId = "";
                int count = 0;
                if (id.IndexOf("_") == -1) // id中不包含_，代表顶级分类
                {
                    count = treeView1.Nodes.Count - 1;
                }
                else
                {
                    count = treeView1.SelectedNode.Parent.Nodes.Count - 1;
                }
                if (index < count)
                {
                    TreeNode node = (TreeNode)treeView1.SelectedNode.Clone();
                    if (id.IndexOf("_") == -1) // id中不包含_，代表顶级分类
                    {
                        exchangeId = treeView1.Nodes[index + 1].Name;
                        treeView1.Nodes.Insert(index + 2, node);
                    }
                    else
                    {
                        exchangeId = treeView1.SelectedNode.Parent.Nodes[index + 1].Name;
                        treeView1.SelectedNode.Parent.Nodes.Insert(index + 2, node);
                    }
                    treeView1.SelectedNode.Remove();
                    selectedNode(id);
                    updateTreeOrder(id, exchangeId);
                }
            }
        }
        /// <summary>
        /// 获取网络图标
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_get_icon_Click(object sender, EventArgs e)
        {
            if(currSelectedNode != null){
                TreeObj tree = getTreeObj(currSelectedNode.Name);
                if (tree != null)
                {
                    String url = tree.address;
                    if(!String.IsNullOrEmpty(url)){
                        String ico = YSTools.YSWebNet.GetHostUrlByURL(url) + "favicon.ico";
                        if(!Directory.Exists(Utils.baseDir + "icon")){
                            Directory.CreateDirectory(Utils.baseDir + "icon");
                        }
                        String icoLocal = Utils.baseDir + "icon\\favicon" + tree.id + ".ico";
                        try {

                            icoLocal = YSTools.YSWebNet.DownLoadWebFile(ico, icoLocal);

                            if (icoLocal != null && File.Exists(icoLocal))
                            {
                                int index = imageList1.Images.Count;
                            
                                imageList1.Images.Add(Image.FromFile(icoLocal));
                                tree.imageIndex = index;
                                currSelectedNode.ImageIndex = index;
                                currSelectedNode.SelectedImageIndex = index;
                                Utils.appImageList.Add(icoLocal);
                            }                            
                        }
                        catch
                        {
                            Utils.appImageList.Remove(icoLocal);
                            File.Delete(icoLocal);
                        }
                    }
                }
            }            
        }

        /// <summary>
        /// 设置本地图标
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_set_local_Click(object sender, EventArgs e)
        {
            if(currSelectedNode != null){
                OpenFileDialog openFileDialog = new OpenFileDialog();
                //openFileDialog.InitialDirectory = "c:\\";//注意这里写路径时要用c:\\而不是c:\
                openFileDialog.Filter = "图标(*.ico;*.png)|*.ico;*.png";
                openFileDialog.RestoreDirectory = true;
                openFileDialog.FilterIndex = 1;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    TreeObj tree = getTreeObj(currSelectedNode.Name);
                    String icoPath = openFileDialog.FileName;
                    String newFile = Utils.baseDir + "icon\\favicon" + tree.id + ".ico";
                    try
                    {                        
                        int index = imageList1.Images.Count;                                                
                        File.Copy(icoPath, newFile);
                        Image img = Image.FromFile(newFile);
                        imageList1.Images.Add(img);
                        tree.imageIndex = index;
                        currSelectedNode.ImageIndex = index;
                        currSelectedNode.SelectedImageIndex = index;
                        Utils.appImageList.Add(newFile);
                    }
                    catch
                    {
                        if (File.Exists(newFile))
                        {
                            File.Delete(newFile);
                        }
                    }
                    
                }
            }            
        }

        /// <summary>
        /// 重置图标
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menu_reset_icon_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                String id = treeView1.SelectedNode.Name;
                TreeObj tree = getTreeObj(id);
                if (id.IndexOf("_") != -1) // 非顶级节点
                {
                    int sonCount = treeView1.SelectedNode.Nodes.Count;
                    if (sonCount > 0) // 非顶级父节点
                    {
                        tree.imageIndex = 1;
                        treeView1.SelectedNode.ImageIndex = 1;
                        treeView1.SelectedNode.SelectedImageIndex = 1;
                    }
                    else  // 非顶级子节点
                    {
                        tree.imageIndex = 2;
                        treeView1.SelectedNode.ImageIndex = 2;
                        treeView1.SelectedNode.SelectedImageIndex = 2;
                    }
                }
                else// 顶级节点
                {
                    tree.imageIndex = 0;
                    treeView1.SelectedNode.ImageIndex = 0;
                    treeView1.SelectedNode.SelectedImageIndex = 0;
                }
            } 
        }

        /// <summary>
        /// 监听窗口按键
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            Console.WriteLine("===>" + e.KeyChar.ToString());
        }

        private void 展开所有ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            treeView1.ExpandAll();
        }

        private void 收缩所有ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            treeView1.CollapseAll();
            if (treeView1.SelectedNode != null)
            {
                currSelectedNode = treeView1.SelectedNode;
            }
        }

        /// <summary>
        /// 功能按钮点击
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_func(object sender, EventArgs e)
        {
            PictureBox but = (PictureBox)sender;
            String name = but.Name;
            String tag2 = name.Substring(0, name.Length - 1);
            String tag1 = name.Substring(name.Length - 1);
            if(tag1.Equals("1")){
                if (tag2.Equals("copyacc"))
                {
                    copyText(account1);           
                }
                else if (tag2.Equals("copypwd"))
                {
                    copyText(password1);
                }
                else if (tag2.Equals("showPBox"))
                {
                    int index = Convert.ToInt32(tag1) - 1;
                    changeBtnIcon(password1, index);
                }
            }
            else if (tag1.Equals("2"))
            {
                if (tag2.Equals("copyacc"))
                {
                    copyText(account2);    
                }
                else if (tag2.Equals("copypwd"))
                {
                    copyText(password2);
                }
                else if (tag2.Equals("showPBox"))
                {
                    int index = Convert.ToInt32(tag1) - 1;
                    changeBtnIcon(password2, index);
                }
            }
            else if (tag1.Equals("3"))
            {
                if (tag2.Equals("copyacc"))
                {
                    copyText(account3);    
                }
                else if (tag2.Equals("copypwd"))
                {
                    copyText(password3);
                }
                else if (tag2.Equals("showPBox"))
                {
                    int index = Convert.ToInt32(tag1) - 1;
                    changeBtnIcon(password3, index);
                }
            }
            else if (tag1.Equals("4"))
            {
                if (tag2.Equals("copyacc"))
                {
                    copyText(account4);    
                }
                else if (tag2.Equals("copypwd"))
                {
                    copyText(password4);
                }
                else if (tag2.Equals("showPBox"))
                {
                    int index = Convert.ToInt32(tag1) - 1;
                    changeBtnIcon(password4, index);
                }
            }
            else if (tag1.Equals("5"))
            {
                if (tag2.Equals("copyacc"))
                {
                    copyText(account5);    
                }
                else if (tag2.Equals("copypwd"))
                {
                    copyText(password5);
                }
                else if (tag2.Equals("showPBox"))
                {
                    int index = Convert.ToInt32(tag1) - 1;
                    changeBtnIcon(password5, index);
                }
            }
        }

        public void changeBtnIcon(Control trol, int index)
        {
            TextBox pass = (TextBox) trol;
            if (pwdShow[index] == 1)
            {
                pwdShow[index] = 2;
                pass.PasswordChar = '\0';
            }
            else
            {
                pwdShow[index] = 1;
                pass.PasswordChar = '◆';
            }
        }

        public void showPanelAnimote(int type)
        {
            bool enabled = false;
            if(type == 1){ // 解禁
                enabled = true;
            }
            textBox_name.Text = enabled ? textBox_name.Text : "";
            /*textBox_name.Enabled = enabled;            
            sync_box.Enabled = enabled;*/
            textBox_name.Visible = enabled;
            sync_box.Visible = enabled;
            label1.Visible = enabled;

            textBox_adress.Text = enabled ? textBox_adress.Text : "";
            /*textBox_adress.Enabled = enabled;            
            goto_box.Enabled = enabled;*/
            textBox_adress.Visible = enabled;
            goto_box.Visible = enabled;
            label2.Visible = enabled;

            account1.Text = enabled ? account1.Text : "";
            /*account1.Enabled = enabled;            
            copyacc1.Enabled = enabled;*/
            account1.Visible = enabled;
            copyacc1.Visible = enabled;
            groupBox1.Visible = enabled;

            password1.Text = enabled ? password1.Text : "";
            /*password1.Enabled = enabled;            
             showPBox1.Enabled = enabled;
             copypwd1.Enabled = enabled;*/
            copypwd1.Visible = enabled;
            password1.Visible = enabled;            
            showPBox1.Visible = enabled;

            account2.Text = enabled ? account2.Text : "";
            /*account2.Enabled = enabled;
            copyacc2.Enabled = enabled;*/
            account2.Visible = enabled;
            copyacc2.Visible = enabled;
            groupBox2.Visible = enabled;

            password2.Text = enabled ? password2.Text : "";
            /*password2.Enabled = enabled;
            showPBox2.Enabled = enabled;
            copypwd2.Enabled = enabled;*/
            copypwd2.Visible = enabled;
            password2.Visible = enabled;
            showPBox2.Visible = enabled;

            account3.Text = enabled ? account3.Text : "";
            /*account3.Enabled = enabled;
            copyacc3.Enabled = enabled;*/
            account3.Visible = enabled;
            copyacc3.Visible = enabled;
            groupBox3.Visible = enabled;

            password3.Text = enabled ? password3.Text : "";
            /*password3.Enabled = enabled;
            copypwd3.Enabled = enabled;
            showPBox3.Enabled = enabled;*/
            password3.Visible = enabled;
            copypwd3.Visible = enabled;
            showPBox3.Visible = enabled;

            account4.Text = enabled ? account4.Text : "";
            /*account4.Enabled = enabled;
            copyacc4.Enabled = enabled;*/
            copyacc4.Visible = enabled;
            account4.Visible = enabled;
            groupBox4.Visible = enabled;

            password4.Text = enabled ? password4.Text : "";
            /*password4.Enabled = enabled;
            copypwd4.Enabled = enabled;
            showPBox4.Enabled = enabled;*/
            password4.Visible = enabled;
            copypwd4.Visible = enabled;
            showPBox4.Visible = enabled;

            account5.Text = enabled ? account5.Text : "";
            /*account5.Enabled = enabled;
            copyacc5.Enabled = enabled;*/
            account5.Visible = enabled;            
            copyacc5.Visible = enabled;
            groupBox5.Visible = enabled;

            password5.Text = enabled ? password5.Text : "";
            /*password5.Enabled = enabled;
            showPBox5.Enabled = enabled;
            copypwd5.Enabled = enabled;*/
            password5.Visible = enabled;
            copypwd5.Visible = enabled;
            showPBox5.Visible = enabled;

            //richTextBox1.Enabled = enabled;

            rich_back.Visible = enabled;            
            richTextBox1.Visible = enabled;

            if (enabled)
            {
                rich_back.Image = u.GetImage("richtext");
            }
            else
            {
                rich_back.Image = u.GetImage("richtext_enable");
            }            
        }
        
        /// <summary>
        /// 复制文本
        /// </summary>
        /// <param name="trol"></param>
        public void copyText(Control trol)
        {
            try
            {
                if (String.IsNullOrEmpty(trol.Text))
                {
                    toolTip1.Show("没有可复制的内容", trol, 5, -40);
                }
                else
                {
                    Clipboard.SetText(trol.Text);
                    toolTip1.Show("复制成功", trol, 5, -40);
                }
                ThreadPool.QueueUserWorkItem((a) =>
                {
                    MyDelegate md2 = new MyDelegate(hideToolTip);
                    Thread.Sleep(2000);
                    Invoke(md2, trol);
                });
            }
            catch { }            
        }

        public void hideToolTip(Control trol)
        {
            toolTip1.Hide(trol);
        }

        private void treeView1_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                String  id = treeView1.SelectedNode.Name;
                ThreadPool.QueueUserWorkItem((a) => {
                    Thread.Sleep(10);
                    TreeDelegate td = new TreeDelegate(updateTree);
                    Invoke(td, 1, id);                    
                });
            }
        }

        // 修改树数据
        public void updateTree(int type, String id)
        {
            if(type == 1){
                TreeObj tree = getTreeObj(id);
                tree.treeText = treeView1.SelectedNode.Text;
            } else if(type == 2){
                
            }
        }
        /// <summary>
        ///  修改顺序
        /// </summary>
        public void updateTreeOrder(String id, String exId)
        {
            TreeObj tree = null;
            int index = -1, index2 = -1;
            for (int i = 0; i < treeList.Count; i++)
            {
                tree = treeList[i];
                if (tree.id.Equals(id))
                {
                    index = i;
                }
                else if (tree.id.Equals(exId))
                {
                    index2 = i;
                }
            }
            if(index != -1 && index2 != -1){
                TreeObj temp = treeList[index];
                treeList[index] = treeList[index2];
                treeList[index2] = temp;
            }
        }

        private void treeView1_Leave(object sender, EventArgs e)
        {
            if (currSelectedNode != null)
            {
                currSelectedNode.ForeColor = Color.Red;
            }
        }

        private void treeView1_Enter(object sender, EventArgs e)
        {
            if (currSelectedNode != null)
            {
                currSelectedNode.ForeColor = Color.Black;
            }
        }

        private void treeView1_MouseLeave(object sender, EventArgs e)
        {
            currSelectedNode = treeView1.SelectedNode;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (currSelectedNode != null)
            {
                textBox_name.Text = currSelectedNode.Text;
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(textBox_adress.Text))
            {
                try
                {
                    Process.Start(textBox_adress.Text);
                }
                catch { }
            }            
        }

        /// <summary>
        /// 根据ID获取数对象
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TreeObj getTreeObj(String id)
        {
            foreach (TreeObj tree in treeList)
            {
                if (tree.id.Equals(id))
                {
                    return tree;
                }
            }
            return null;
        }

        /// <summary>
        /// 删除树对象级子类
        /// </summary>
        /// <param name="id"></param>
        public void removeTree(String id)
        {
            TreeObj tree = null;
            for (int i = 0; i < treeList.Count; i++ )
            {
                tree = treeList[i];
                if (tree.id.Equals(id))
                {
                    treeList.Remove(tree);
                }
                else if (tree.parentId.Equals(id))
                {
                    removeTree(tree.id);
                }
            }
        }

        public void removeTreeAll()
        {
            treeView1.Nodes.Clear();
            treeList.Clear();
        }

        private void 保存数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(!saveIng){
                saveIng = true;
                u.saveData();
                panel4.Visible = true;

                ThreadPool.QueueUserWorkItem((a) =>
                {
                    Thread.Sleep(3000);
                    saveIng = false;
                    ShowDelegate sd = new ShowDelegate(showTipPanel);
                    Invoke(sd);
                });
            }
        }

        public void showTipPanel()
        {
            panel4.Visible = false;
            saveIng = false;
        }

        /// <summary>
        /// 初始化树
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        public void initTree(String id, int type)
        {
            foreach (TreeObj tree in treeList)
            {
                if(tree.parentId.Equals(id)){
                    if (type == 0)
                    { 
                        // 一级分类
                        treeView1.Nodes.Add(tree.id, tree.treeText, tree.imageIndex, tree.imageIndex);
                    }
                    else // 子类
                    {
                        selectedNode(id);
                        if (treeView1.SelectedNode != null)
                        {
                            if (id.IndexOf("_") != -1)
                            {
                                treeView1.SelectedNode.ImageIndex = 1;
                                treeView1.SelectedNode.SelectedImageIndex = 1;
                            }
                            treeView1.SelectedNode.Nodes.Add(tree.id, tree.treeText, tree.imageIndex, tree.imageIndex);
                        }
                    }
                    initTree(tree.id, 1);
                }
            }
        }

        
        private void TextBoxTextChanged(object sender, EventArgs e)
        {
            if (currNodeId != null)
            {
                TreeObj tree = getTreeObj(currNodeId);
                if(tree == null){
                    return;
                }
                TextBox tb = (TextBox) sender;
                if (tb.Name.Equals("textBox_name"))
                {
                    tree.name = textBox_name.Text;
                }
                else if (tb.Name.Equals("textBox_adress"))
                {
                    tree.address = textBox_adress.Text;
                }
                else if (tb.Name.Equals("account1"))
                {
                    tree.account1 = account1.Text;
                }
                else if (tb.Name.Equals("account2"))
                {
                    tree.account2 = account2.Text;
                }
                else if (tb.Name.Equals("account3"))
                {
                    tree.account3 = account3.Text;
                }
                else if (tb.Name.Equals("account4"))
                {
                    tree.account4 = account4.Text;
                }
                else if (tb.Name.Equals("account5"))
                {
                    tree.account5 = account5.Text;
                }
                else if (tb.Name.Equals("password1"))
                {
                    tree.password1 = password1.Text;
                }
                else if (tb.Name.Equals("password2"))
                {
                    tree.password2 = password2.Text;
                }
                else if (tb.Name.Equals("password3"))
                {
                    tree.password3 = password3.Text;
                }
                else if (tb.Name.Equals("password4"))
                {
                    tree.password4 = password4.Text;
                }
                else if (tb.Name.Equals("password5"))
                {
                    tree.password5 = password5.Text;
                }
            }            
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            TreeObj tree = getTreeObj(currNodeId);
            tree.remark = richTextBox1.Text;
        }

        private void 退出ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //u.ExitApplication();
            this.Hide();
            Thread.Sleep(200);
            ThreadPool.QueueUserWorkItem((a) =>
            {
                u.ExitApplication();
            });
        }

        private void 导入数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("此操作将会使用您备份的数据替换当前数据\n您确定要还原吗？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                // 导入还原
                OpenFileDialog openFileDialog = new OpenFileDialog();
                //openFileDialog.InitialDirectory = "c:\\";//注意这里写路径时要用c:\\而不是c:\
                openFileDialog.Filter = "数据包(*.yar)|*.yar";
                openFileDialog.RestoreDirectory = true;
                openFileDialog.FilterIndex = 1;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    String fName = openFileDialog.FileName;
                    String data = YSTools.YSFile.readFileToString(fName, true, Utils.cdkey);
                    data = YSTools.YSEncrypt.DecryptB(data, Utils.abkey);
                    byte[] objByte = Convert.FromBase64String(data);
                    if (objByte.Length > 0)
                    {
                        Object obj = YSTools.YSConvert.ByteToObject(objByte);
                        if (obj != null)
                        {
                            List<TreeObj> list = (List<TreeObj>)obj;
                            if (list.Count > 0)
                            {
                                removeTreeAll();
                                treeList = list;
                                initTree("_", 0);
                            }
                        }
                        MessageBox.Show("还原成功");
                    }
                    else
                    {
                        MessageBox.Show("还原失败，原因：数据被损坏");
                    }                    
                }
            }
        }

        

        private void 导出数据ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 导出
            SaveFileDialog sfd = new SaveFileDialog();
            //设置文件类型 
            sfd.Filter = "数据包（*.yar）|*.yar";

            //保存对话框是否记忆上次打开的目录 
            sfd.RestoreDirectory = true;

            //点了保存按钮进入 
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string localFilePath = sfd.FileName.ToString(); //获得文件路径

                    if (File.Exists(localFilePath))
                    {
                        File.Delete(localFilePath);
                    }
                    byte[] objByte = YSTools.YSConvert.ObjectToByte(treeList);
                    String data = Convert.ToBase64String(objByte);
                    data = YSTools.YSEncrypt.EncryptA(data, Utils.abkey);
                    YSTools.YSFile.writeFileByString(localFilePath, data, true, Utils.cdkey);
                    String FilePath = localFilePath.Substring(0, localFilePath.LastIndexOf("\\"));
                    System.Diagnostics.Process.Start("explorer.exe", FilePath);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("备份失败，原因：" + ex.Message);
                }                
            }  
        }

        private void 关于ToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            AboutUS au = new AboutUS();
            au.ShowDialog();
        }

        private void 官网ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://yisin.88448.com/viewthread.php?tid=53367");
        }

        private void 使用说明ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Use use = new Use();
            use.ShowDialog();
        }

        private void 显示窗口ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //判断是否已经最小化于托盘 
            if(this.Visible == false)
            {
                // 显示保险箱
                ShowHideForm(true);             
            }
            else
            {
                // 隐藏保险箱达到最小化目的
                ShowHideForm(false);
            }
        }

        private void 查找ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 关闭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void close_box_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;
            if (me.Button == MouseButtons.Left)
            {
                Form1_FormClosing(null, null);
            }
        }

        private void box_mouse_enter(object sender, EventArgs e)
        {
            PictureBox box = (PictureBox)sender;
            String tag = box.Tag.ToString();
            box.Image = u.GetImageBySkin(tag, 1);
        }

        private void box_mouse_leave(object sender, EventArgs e)
        {
            PictureBox box = (PictureBox)sender;
            String tag = box.Tag.ToString();
            box.Image = u.GetImageBySkin(tag);
        }

        private void box_mouse_down(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left){
                PictureBox box = (PictureBox)sender;
                String tag = box.Tag.ToString();
                box.Image = u.GetImageBySkin(tag, 2);
            }            
        }

        private void box_mouse_up(object sender, MouseEventArgs e)
        {
            PictureBox box = (PictureBox)sender;
            String tag = box.Tag.ToString();
            box.Image = u.GetImageBySkin(tag, 1);
        }

        private void min_box_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;
            if (me.Button == MouseButtons.Left)
            {
                // 隐藏保险箱达到最小化目的
                ShowHideForm(false);
            }
        }

        private void richTextBox1_Enter(object sender, EventArgs e)
        {
            rich_back.Image = u.GetImage("richtext_active");
        }

        private void richTextBox1_Leave(object sender, EventArgs e)
        {
            rich_back.Image = u.GetImage("richtext");
        }

        private void menu_box_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;
            if (me.Button == MouseButtons.Left)
            {
                contextMenu.Show(menu_box, new Point(me.X, me.Y));
            }
        }


        Point mouseOff;//鼠标移动位置变量
        bool leftFlag;//标签是否为左键
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y); //得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
            Utils.SetLastOparateTime();

            if(skinined){
                panel_skin.Visible = false;
                skinined = false;                
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }
        }

        private void 锁定保险箱ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            u.LockStrongBox(true);
        }

        private void skin_box_Click(object sender, EventArgs e)
        {
            panel_skin.Visible = true;
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                // 隐藏保险箱
                //ShowHideForm(false);
            }
            else if (this.WindowState == FormWindowState.Normal)
            {
                // 显示保险箱
                //ShowHideForm(true);
            }
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (islock)
                {
                    ShowHideLockScreen(!ls.Visible);
                }
                else
                {
                    // 显示与隐藏保险箱
                    ShowHideForm(!this.Visible);
                }
            }
        }

        /// <summary>
        /// 显示与隐藏Form
        /// </summary>
        /// <param name="flag"></param>
        public void ShowHideForm(bool flag)
        {
            //最小化窗体
            this.Visible = flag;
            //隐藏任务栏区图标 
            this.ShowInTaskbar = flag;
            // 置前
            this.TopMost = flag;

            显示窗口ToolStripMenuItem.Text = flag ? "隐藏窗口" : "显示窗口";

        }

        /// <summary>
        /// 锁定与解锁Form时 flag=true锁定
        /// </summary>
        /// <param name="flag"></param>
        public void LockOrNoForm(bool flag)
        {
            islock = flag;
            //还原窗体显示 
            this.Visible = !islock;
            //任务栏区显示图标 
            this.ShowInTaskbar = !islock;

            锁定ToolStripMenuItem1.Text = islock ? "解锁" : "锁定";
            显示窗口ToolStripMenuItem.Visible = !islock;
        }

        /// <summary>
        /// 显示与隐藏锁屏窗口
        /// </summary>
        /// <param name="flag"></param>
        public void ShowHideLockScreen(bool flag)
        {
            ls.changeLockStatus(flag);
        }

        private void 开机启动ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (Utils.autoStart.Equals("1"))
                {
                    Regedit.SetAutoRun(Application.ExecutablePath, true);
                    Utils.autoStart = "2";
                    开机启动ToolStripMenuItem.Checked = true;
                }
                else
                {
                    Regedit.SetAutoRun(Application.ExecutablePath, false);
                    Utils.autoStart = "1";
                    开机启动ToolStripMenuItem.Checked = false;
                }                
            }
            catch(Exception)
            {
                Utils.autoStart = "1";
                开机启动ToolStripMenuItem.Checked = false;
                MessageBox.Show("无操作员权限，无法设置开机启动\n\n请使用管理员权限运行本软件后，再设置开机启动");
            }
        }

        private void 加密文件ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            //openFileDialog.InitialDirectory = "c:\\";//注意这里写路径时要用c:\\而不是c:\
            openFileDialog.Filter = "选择需要加密的文件(*.*)|*.*";
            openFileDialog.RestoreDirectory = true;
            openFileDialog.FilterIndex = 1;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                String toFile = null;
                try
                {
                    String fName = openFileDialog.FileName;
                    FileInfo file = new FileInfo(fName);
                    String fileName = file.Name.Substring(0, file.Name.IndexOf(file.Extension));
                    toFile = file.DirectoryName + "\\" + fileName + "_E." + file.Extension;
                    YSTools.YSFile.EncryptFile(fName, toFile, Utils.filekey);
                    MessageBox.Show("加密成功：" + toFile);
                }
                catch(Exception ex)
                {
                    if (toFile != null && File.Exists(toFile))
                    {
                        File.Delete(toFile);
                    }
                    MessageBox.Show("加密失败：" + ex.Message);
                }                
            }
        }

        private void 文件解密ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            //openFileDialog.InitialDirectory = "c:\\";//注意这里写路径时要用c:\\而不是c:\
            openFileDialog.Filter = "选择需要加密的文件(*.*)|*.*";
            openFileDialog.RestoreDirectory = true;
            openFileDialog.FilterIndex = 1;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                String toFile = null;
                try
                {
                    String fName = openFileDialog.FileName;
                    FileInfo file = new FileInfo(fName);
                    String fileName = file.Name.Substring(0, file.Name.IndexOf(file.Extension));
                    toFile = file.DirectoryName + "\\" + fileName + "_D" + file.Extension;
                    YSTools.YSFile.DecryptFile(fName, toFile, Utils.filekey);
                    MessageBox.Show("解密成功：" + toFile);
                }
                catch (Exception ex)
                {
                    if (toFile != null && File.Exists(toFile))
                    {
                        File.Delete(toFile);
                    }
                    MessageBox.Show("解密失败：" + ex.Message);
                }
            }
        }

        private void 修改密码ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SetPwd sp = new SetPwd();
            sp.ShowDialog();
        }

        private void feedback_box_Click(object sender, EventArgs e)
        {
            //Process.Start(h ttp://mail.qq.com/cgi-bin/qm_share?t=qm_mailme&email=D3ZmYXdmYTw8PTlPeWZ-IX5_IWxgYg);
            Feedback fd = new Feedback();
            fd.ShowDialog();
        }

        private void treeView1_MouseMove(object sender, MouseEventArgs e)
        {
            Utils.SetLastOparateTime();
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            Utils.SetLastOparateTime();

            if (skinined)
            {
                panel_skin.Visible = false;
                skinined = false;
            }
        }

        private void richTextBox1_MouseMove(object sender, MouseEventArgs e)
        {
            Utils.SetLastOparateTime();
        }

        private void skin_box_MouseEnter(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            String tag = pb.Tag.ToString();
            if(!tag.Equals("none")){
                pb.Image = u.GetImage(tag, 1);
                if(tag.Equals("skinbox_green")){
                    choosebox_green.Visible = true;
                }
                else if (tag.Equals("skinbox_blue"))
                {
                    choosebox_blue.Visible = true;
                }
                else if (tag.Equals("skinbox_blank"))
                {
                    choosebox_blank.Visible = true;
                }
                else if (tag.Equals("skinbox_red"))
                {
                    choosebox_red.Visible = true;
                }
                else if (tag.Equals("skinbox_silver"))
                {
                    choosebox_silver.Visible = true;
                }
            }
        }

        private void skin_box_MouseLeave(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            String tag = pb.Tag.ToString();
            if (!tag.Equals("none"))
            {
                pb.Image = u.GetImage(tag, 0);

                choosebox_green.Visible = false;
                choosebox_blue.Visible = false;
                choosebox_red.Visible = false;
                choosebox_blank.Visible = false;
                choosebox_silver.Visible = false;
            }
        }

        private void boxskin_Click(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            String tag = pb.Tag.ToString();
            if (tag.Equals("skinbox_green"))
            {
                Utils.skin = "green";
                changeSkin();
            }
            else if (tag.Equals("skinbox_blue"))
            {
                Utils.skin = "blue";
                changeSkin();
            }
            else if (tag.Equals("skinbox_blank"))
            {
                Utils.skin = "blank";
                changeSkin();
            }
            else if (tag.Equals("skinbox_red"))
            {
                Utils.skin = "red";
                changeSkin();
            }
            else if (tag.Equals("skinbox_silver"))
            {
                Utils.skin = "silver";
                changeSkin();
            }
        }

        bool skinined = false;
        private void panel_skin_MouseEnter(object sender, EventArgs e)
        {
            skinined = true;
        }

        private void label11_Click(object sender, EventArgs e)
        {
            treeView1.ExpandAll();
        }

        private void label12_Click(object sender, EventArgs e)
        {
            收缩所有ToolStripMenuItem_Click(null, null);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            String key = searchKey.Text;
            if (!String.IsNullOrEmpty(key))
            {
                TreeObj obj = null, tree = null;
                for (int i =0; i< treeList.Count; i++)
                {
                    tree = treeList[i];
                    if (tree != null && !String.IsNullOrEmpty(tree.treeText) && tree.treeText.IndexOf(key) != -1)
                    {
                        obj = tree;
                        break;
                    }
                }
                if (obj != null)
                {
                    if (currSelectedNode != null)
                    {
                        currSelectedNode.ForeColor = Color.Black;
                        selectedNode(obj.id, 1);
                        if (currSelectedNode != null)
                        {
                            currSelectedNode.EndEdit(false);
                        }
                    }
                }
            }
            
        }

        private void treetool_MouseEnter(object sender, EventArgs e)
        {
            Label lal = (Label)sender;
            lal.ForeColor = Color.Red;
        }

        private void treetool_MouseLeave(object sender, EventArgs e)
        {
            Label lal = (Label)sender;
            lal.ForeColor = Color.FromArgb(255, 60, 41, 153);
        }

        private void searchKey_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter){
                String key = searchKey.Text;
                if (!String.IsNullOrEmpty(key))
                {
                    TreeObj obj = null, tree = null;
                    for (int i = 0; i < treeList.Count; i++)
                    {
                        tree = treeList[i];
                        if (tree != null && !String.IsNullOrEmpty(tree.treeText) && tree.treeText.ToLower().IndexOf(key.ToLower()) != -1)
                        {
                            obj = tree;
                            break;
                        }
                    }
                    if (obj != null)
                    {
                        if (currSelectedNode != null)
                        {
                            currSelectedNode.ForeColor = Color.Black;
                            selectedNode(obj.id, 1);
                            if (currSelectedNode != null)
                            {
                                currSelectedNode.EndEdit(false);
                            }
                        }
                    }
                }
            } else if(e.KeyCode == Keys.S && e.Control){
                保存数据ToolStripMenuItem_Click(null, null);
            }
        }

        private void Form_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.S && e.Control)
            {
                保存数据ToolStripMenuItem_Click(null, null);
            }
        }
                      

    }


    [Serializable]
    public class TreeObj
    {
        public String id { get; set; }
        public int order { get; set; }
        public int imageIndex { get; set; }
        public String treeText { get; set; }
        public String parentId { get; set; }
        public String name { get; set; }
        public String address { get; set; }
        public String remark { get; set; }
        public String account1 { get; set; }
        public String account2 { get; set; }
        public String account3 { get; set; }
        public String account4 { get; set; }
        public String account5 { get; set; }
        public String password1 { get; set; }
        public String password2 { get; set; }
        public String password3 { get; set; }
        public String password4 { get; set; }
        public String password5 { get; set; }
    }


}
