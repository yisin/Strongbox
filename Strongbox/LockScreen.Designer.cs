﻿namespace Strongbox
{
    partial class LockScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LockScreen));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lock_ok = new System.Windows.Forms.PictureBox();
            this.cmenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.showhideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出保险箱ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.yaoshi = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lock_ok)).BeginInit();
            this.cmenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.yaoshi)).BeginInit();
            this.SuspendLayout();
            // 
            // lock_ok
            // 
            this.lock_ok.BackColor = System.Drawing.Color.Transparent;
            this.lock_ok.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.lock_ok.ContextMenuStrip = this.cmenu;
            this.lock_ok.Location = new System.Drawing.Point(295, 4);
            this.lock_ok.Name = "lock_ok";
            this.lock_ok.Size = new System.Drawing.Size(52, 52);
            this.lock_ok.TabIndex = 0;
            this.lock_ok.TabStop = false;
            this.lock_ok.Tag = "lock";
            this.lock_ok.Click += new System.EventHandler(this.lock_ok_Click);
            this.lock_ok.MouseDown += new System.Windows.Forms.MouseEventHandler(this.box_mouse_down);
            this.lock_ok.MouseEnter += new System.EventHandler(this.box_mouse_enter);
            this.lock_ok.MouseLeave += new System.EventHandler(this.box_mouse_leave);
            this.lock_ok.MouseUp += new System.Windows.Forms.MouseEventHandler(this.box_mouse_up);
            // 
            // cmenu
            // 
            this.cmenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showhideToolStripMenuItem,
            this.退出保险箱ToolStripMenuItem});
            this.cmenu.Name = "cmenu";
            this.cmenu.Size = new System.Drawing.Size(137, 48);
            // 
            // showhideToolStripMenuItem
            // 
            this.showhideToolStripMenuItem.Name = "showhideToolStripMenuItem";
            this.showhideToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.showhideToolStripMenuItem.Text = "隐藏窗口";
            this.showhideToolStripMenuItem.Click += new System.EventHandler(this.showhideToolStripMenuItem_Click);
            // 
            // 退出保险箱ToolStripMenuItem
            // 
            this.退出保险箱ToolStripMenuItem.Name = "退出保险箱ToolStripMenuItem";
            this.退出保险箱ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.退出保险箱ToolStripMenuItem.Text = "退出保险箱";
            this.退出保险箱ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.ContextMenuStrip = this.cmenu;
            this.textBox1.Font = new System.Drawing.Font("宋体", 16F);
            this.textBox1.ForeColor = System.Drawing.Color.Green;
            this.textBox1.Location = new System.Drawing.Point(29, 18);
            this.textBox1.Name = "textBox1";
            this.textBox1.PasswordChar = '★';
            this.textBox1.Size = new System.Drawing.Size(263, 25);
            this.textBox1.TabIndex = 1;
            this.textBox1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.label1.ContextMenuStrip = this.cmenu;
            this.label1.Font = new System.Drawing.Font("宋体", 12F);
            this.label1.ForeColor = System.Drawing.Color.Olive;
            this.label1.Location = new System.Drawing.Point(120, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 16);
            this.label1.TabIndex = 2;
            // 
            // yaoshi
            // 
            this.yaoshi.BackColor = System.Drawing.Color.Transparent;
            this.yaoshi.ContextMenuStrip = this.cmenu;
            this.yaoshi.Location = new System.Drawing.Point(3, 18);
            this.yaoshi.Name = "yaoshi";
            this.yaoshi.Size = new System.Drawing.Size(23, 25);
            this.yaoshi.TabIndex = 3;
            this.yaoshi.TabStop = false;
            this.yaoshi.Tag = "yaoshi";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.label2.ContextMenuStrip = this.cmenu;
            this.label2.Font = new System.Drawing.Font("宋体", 11F);
            this.label2.ForeColor = System.Drawing.Color.Olive;
            this.label2.Location = new System.Drawing.Point(120, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 15);
            this.label2.TabIndex = 4;
            // 
            // LockScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(350, 61);
            this.ContextMenuStrip = this.cmenu;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.yaoshi);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lock_ok);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LockScreen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LockScreen";
            this.TransparencyKey = System.Drawing.Color.White;
            this.Load += new System.EventHandler(this.LockScreen_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.lock_ok)).EndInit();
            this.cmenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.yaoshi)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox lock_ok;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox yaoshi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ContextMenuStrip cmenu;
        private System.Windows.Forms.ToolStripMenuItem showhideToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出保险箱ToolStripMenuItem;
    }
}