﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Threading;

namespace Strongbox
{
    public partial class LockScreen : Form
    {
        Utils u = new Utils();

        List<String> passList = new List<string>();
        public static bool initPwd = false;
        private delegate void ShowErrorDelegate(int i);

        public LockScreen()
        {
            InitializeComponent();
            
            changeSkin();
        }

        private void LockScreen_Load(object sender, EventArgs e)
        {
            if(initPwd){
                label1.Text = "首次请设置解锁密码";
                label1.ForeColor = Color.FromArgb(255, 135, 134, 11);
                showhideToolStripMenuItem.Text = "取消锁定";
            }
            timer1.Interval = 1000;
            timer1.Tick += timer1_Tick;
            timer1.Start();
        }

        void timer1_Tick(object sender, EventArgs e)
        {
            if (!initPwd && textBox1.Text.Length < 1)
            {
                DateTime dt = DateTime.Now;
                String str = String.Format("{0}-{1:00}-{2} {3:00}:{4:00}:{5:00}", dt.Year, dt.Month, dt.Day,
                    dt.Hour, dt.Minute, dt.Second, dt.DayOfWeek, dt.DayOfYear);

                label1.Text = str;
            }
            
        }

        public void changeSkin()
        {
            lock_ok.Image = u.GetImageBySkin("lock", 0);
            this.BackgroundImage = u.GetImageBySkin("lock_back");
            yaoshi.Image = u.GetImageBySkin("yaoshi");

            if(Utils.skin.Equals("silver")){
                textBox1.ForeColor = Color.FromArgb(255, 178, 176, 20);
            }
            else if(Utils.skin.Equals("red")){
                textBox1.ForeColor = Color.FromArgb(255, 234, 55, 68);
            }
            else if (Utils.skin.Equals("blue"))
            {
                textBox1.ForeColor = Color.FromArgb(255, 53, 66, 148);
            }
            else if (Utils.skin.Equals("green"))
            {
                textBox1.ForeColor = Color.FromArgb(255, 60, 41, 153);
            }
            else if (Utils.skin.Equals("blank"))
            {
                textBox1.ForeColor = Color.FromArgb(255, 25, 25, 25);
            }
        }

        bool isshow = false;
        private void ShowErrorCss(int i)
        {
            if(i < 11){
                yaoshi.Image = u.GetImageBySkin("yaoshi", i % 2);
            }
            if(i >= 30){
                label2.Text = "";
                if (isshow)
                {
                    label1.Visible = true;
                }
            }
        }

        public void ShowErrorCssThread()
        {
            isshow = false;
            if(label1.Visible){
                label1.Visible = false;
                isshow = true;
            }
            ThreadPool.QueueUserWorkItem((a) =>
            {
                for (int i = 0; i < 31; i++)
                {
                    ShowErrorDelegate sed = new ShowErrorDelegate(ShowErrorCss);
                    Invoke(sed, i);
                    Thread.Sleep(50);
                }
            });            
        }

        private void box_mouse_enter(object sender, EventArgs e)
        {
            PictureBox box = (PictureBox)sender;
            String tag = box.Tag.ToString();
            box.Image = u.GetImageBySkin(tag, 1);
        }

        private void box_mouse_leave(object sender, EventArgs e)
        {
            PictureBox box = (PictureBox)sender;
            String tag = box.Tag.ToString();
            box.Image = u.GetImageBySkin(tag, 0);
        }

        private void box_mouse_down(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                PictureBox box = (PictureBox)sender;
                String tag = box.Tag.ToString();
                box.Image = u.GetImageBySkin(tag, 2);
                
            }
        }

        private void box_mouse_up(object sender, MouseEventArgs e)
        {
            PictureBox box = (PictureBox)sender;
            String tag = box.Tag.ToString();
            box.Image = u.GetImageBySkin(tag, 1);
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (textBox1.Text.Length < 1)
            {
                label1.Visible = true;
                if (initPwd)
                {
                    label2.Text = "解锁密码不能少于4位";
                    label2.ForeColor = Color.FromArgb(255, 200, 20, 20);
                    label2.Location = new Point(140, 22);
                    ShowErrorCssThread();
                }
                else
                {
                    label2.Text = "输入解锁密码后方可解锁";
                    label2.ForeColor = Color.FromArgb(255, 200, 20, 20);
                    label2.Location = new Point(120, 22);
                    ShowErrorCssThread();
                }
            }
            else
            {
                label1.Visible = false;
                if (e.KeyCode == Keys.Enter)
                {
                    verifyPwd();
                }
            }
        }

        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //u.ExitApplication();
            this.Hide();
            Thread.Sleep(200);
            ThreadPool.QueueUserWorkItem((a) =>
            {
                u.ExitApplication();
            });
        }


        private void verifyPwd()
        {
            if (initPwd)
            {
                if (textBox1.Text.Length < 4)
                {
                    label2.Text = "解锁密码不能少于4位";
                    label2.ForeColor = Color.FromArgb(255, 200, 20, 20);
                    label2.Location = new Point(140, 22);
                    ShowErrorCssThread();
                }
                else
                {
                    String pwd = YSTools.YSEncrypt.EncryptA(YSTools.YSEncrypt.GetMD5(textBox1.Text), Utils.abkey);
                    Utils.lockPwd = pwd;
                    showhideToolStripMenuItem.Text = "隐藏解锁窗口";
                    changeLockStatus(false);
                    initPwd = false;
                    textBox1.Text = "";
                    Program.form1.LockOrNoForm(false);
                }                
            }
            else
            {
                String pwd1 = Utils.lockPwd;
                String pwd2 = YSTools.YSEncrypt.EncryptA(YSTools.YSEncrypt.GetMD5(textBox1.Text), Utils.abkey);
                if (pwd1.Equals(pwd2))
                {
                    changeLockStatus(false);                 
                    textBox1.Text = "";
                    Program.form1.LockOrNoForm(false);
                }
                else
                {
                    label2.Text = "解锁密码错误";
                    label2.ForeColor = Color.FromArgb(255, 200, 20, 20);
                    label2.Location = new Point(193, 22);
                    ShowErrorCssThread();
                }
            }
        }

        private void showhideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (initPwd)
            {
                changeLockStatus(false);
                Program.form1.ShowHideForm(false);
                showhideToolStripMenuItem.Text = "隐藏窗口";
            }
            else
            {
                changeLockStatus(false);
            }
        }

        private void lock_ok_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;
            if (me.Button == MouseButtons.Left)
            {
                verifyPwd();
            }
        }

        public void changeLockStatus(bool flag)
        {
            this.Visible = flag;
            //隐藏任务栏区图标 
            this.ShowInTaskbar = flag;
            this.TopMost = flag;
        }


        Point mouseOff;//鼠标移动位置变量
        bool leftFlag;//标签是否为左键
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y); //得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }
        }


    }
}
