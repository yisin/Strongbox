﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strongbox
{
    public class Regedit
    {

        public static bool IsRegeditItemExist()
        {
            string[] subkeyNames;
            RegistryKey hkml = Registry.LocalMachine;
            RegistryKey software = hkml.OpenSubKey("SOFTWARE");
            //RegistryKey software = hkml.OpenSubKey("SOFTWARE", true);  
            subkeyNames = software.GetSubKeyNames();
            //取得该项下所有子项的名称的序列，并传递给预定的数组中  
            foreach (string keyName in subkeyNames)
            //遍历整个数组  
            {
                if (keyName == Utils.softName)
                //判断子项的名称  
                {
                    hkml.Close();
                    return true;
                }
            }
            hkml.Close();
            return false;
        }


        public static bool IsRegeditKeyExit()
        {
            string[] subkeyNames;
            RegistryKey hkml = Registry.LocalMachine;
            RegistryKey software = hkml.OpenSubKey("SOFTWARE\\" + Utils.softName);
            //RegistryKey software = hkml.OpenSubKey("SOFTWARE\\test", true);
            subkeyNames = software.GetValueNames();
            //取得该项下所有键值的名称的序列，并传递给预定的数组中
            foreach (string keyName in subkeyNames)
            {
                if (keyName == Utils.softName) //判断键值的名称
                {
                    hkml.Close();
                    return true;
                }

            }
            hkml.Close();
            return false;
        }

        public static void initRegedit()
        {
            if (!IsRegeditItemExist())
            {
                RegistryKey key = Registry.LocalMachine;
                RegistryKey software = key.CreateSubKey("software\\" + Utils.softName, RegistryKeyPermissionCheck.ReadWriteSubTree);
                software.SetValue(Utils.softName, "1");
                key.Close();
            }
        }

        public static void WriteRegeditItemValue(String value)
        {
            RegistryKey key = null;
            RegistryKey software = null;
            if (!IsRegeditItemExist())
            {
                key = Registry.LocalMachine;
                software = key.CreateSubKey("software\\" + Utils.softName, RegistryKeyPermissionCheck.ReadWriteSubTree);
            }
            else
            {
                key = Registry.LocalMachine;
                software = key.OpenSubKey("software\\" + Utils.softName, true); //该项必须已存在
            }
            software.SetValue(Utils.softName, value);
            //在HKEY_LOCAL_MACHINE\SOFTWARE\test下创建一个名为“test”，值为“博客园”的键值。如果该键值原本已经存在，则会修改替换原来的键值，如果不存在则是创建该键值。
            // 注意：SetValue()还有第三个参数，主要是用于设置键值的类型，如：字符串，二进制，Dword等等~~默认是字符串。如：
            // software.SetValue("test", "0", RegistryValueKind.DWord); //二进制信息
            key.Close();
        }

        public static String ReadRegeditItemValue()
        {
            RegistryKey Key = Registry.LocalMachine;
            RegistryKey myreg = Key.OpenSubKey("software\\" + Utils.softName);
            // myreg = Key.OpenSubKey("software\\test",true);
            string info = myreg.GetValue(Utils.softName).ToString();
            myreg.Close();
            return info;
        }

        /// <summary>
        /// 删除
        /// </summary>
        public static void DelRegeditItem()
        {
            RegistryKey delKey = Registry.LocalMachine.OpenSubKey("Software\\" + Utils.softName, true);
            delKey.DeleteValue(Utils.softName);
            delKey.Close();
        }

        /// <summary>
        /// 设置应用程序开机自动运行
        /// </summary>
        /// <param name="fileName">应用程序的文件名</param>
        /// <param name="isAutoRun">是否自动运行，为false时，取消自动运行</param>
        /// <exception cref="System.Exception">设置不成功时抛出异常</exception>
        public static void SetAutoRun(string fileName, bool isAutoRun)
        {
            RegistryKey reg = null;
            try
            {
                if (!System.IO.File.Exists(fileName))
                    throw new Exception("该文件不存在");
                String name = fileName.Substring(fileName.LastIndexOf(@"\") + 1);
                reg = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
                if (reg == null)
                    reg = Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run");
                if (isAutoRun)
                    reg.SetValue(name, fileName);
                else
                    reg.SetValue(name, false);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            finally
            {
                if (reg != null)
                    reg.Close();
            }
        }

    }
}
