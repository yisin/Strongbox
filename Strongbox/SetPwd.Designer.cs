﻿namespace Strongbox
{
    partial class SetPwd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetPwd));
            this.text_old = new System.Windows.Forms.TextBox();
            this.text_new = new System.Windows.Forms.TextBox();
            this.text_renew = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_ok = new System.Windows.Forms.PictureBox();
            this.btn_cancel = new System.Windows.Forms.PictureBox();
            this.info = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.no_label = new System.Windows.Forms.Label();
            this.close_box = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.btn_ok)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_cancel)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.close_box)).BeginInit();
            this.SuspendLayout();
            // 
            // text_old
            // 
            this.text_old.Location = new System.Drawing.Point(103, 18);
            this.text_old.Name = "text_old";
            this.text_old.PasswordChar = '★';
            this.text_old.Size = new System.Drawing.Size(186, 21);
            this.text_old.TabIndex = 0;
            // 
            // text_new
            // 
            this.text_new.Location = new System.Drawing.Point(103, 55);
            this.text_new.Name = "text_new";
            this.text_new.PasswordChar = '★';
            this.text_new.Size = new System.Drawing.Size(186, 21);
            this.text_new.TabIndex = 1;
            // 
            // text_renew
            // 
            this.text_renew.Location = new System.Drawing.Point(103, 92);
            this.text_renew.Name = "text_renew";
            this.text_renew.PasswordChar = '★';
            this.text_renew.Size = new System.Drawing.Size(186, 21);
            this.text_renew.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("宋体", 9F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(47, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "旧密码：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("宋体", 9F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(47, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "新密码：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("宋体", 9F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(23, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "重复新密码：";
            // 
            // btn_ok
            // 
            this.btn_ok.Location = new System.Drawing.Point(80, 166);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(90, 32);
            this.btn_ok.TabIndex = 6;
            this.btn_ok.TabStop = false;
            this.btn_ok.Tag = "btn_ok";
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            this.btn_ok.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseDown);
            this.btn_ok.MouseEnter += new System.EventHandler(this.pictureBox2_MouseEnter);
            this.btn_ok.MouseLeave += new System.EventHandler(this.pictureBox2_MouseLeave);
            this.btn_ok.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseUp);
            // 
            // btn_cancel
            // 
            this.btn_cancel.Location = new System.Drawing.Point(190, 166);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(90, 32);
            this.btn_cancel.TabIndex = 7;
            this.btn_cancel.TabStop = false;
            this.btn_cancel.Tag = "btn_cancel";
            this.btn_cancel.Click += new System.EventHandler(this.btn_cancel_Click);
            this.btn_cancel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseDown);
            this.btn_cancel.MouseEnter += new System.EventHandler(this.pictureBox2_MouseEnter);
            this.btn_cancel.MouseLeave += new System.EventHandler(this.pictureBox2_MouseLeave);
            this.btn_cancel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseUp);
            // 
            // info
            // 
            this.info.AutoSize = true;
            this.info.BackColor = System.Drawing.Color.Transparent;
            this.info.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.info.Location = new System.Drawing.Point(107, 130);
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(0, 12);
            this.info.TabIndex = 8;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.no_label);
            this.panel1.Controls.Add(this.text_renew);
            this.panel1.Controls.Add(this.info);
            this.panel1.Controls.Add(this.text_old);
            this.panel1.Controls.Add(this.btn_cancel);
            this.panel1.Controls.Add(this.text_new);
            this.panel1.Controls.Add(this.btn_ok);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(1, 27);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(356, 213);
            this.panel1.TabIndex = 11;
            // 
            // no_label
            // 
            this.no_label.AutoSize = true;
            this.no_label.BackColor = System.Drawing.Color.Transparent;
            this.no_label.ForeColor = System.Drawing.Color.DarkGray;
            this.no_label.Location = new System.Drawing.Point(109, 22);
            this.no_label.Name = "no_label";
            this.no_label.Size = new System.Drawing.Size(161, 12);
            this.no_label.TabIndex = 9;
            this.no_label.Text = "还未设置过密码，无需旧密码";
            this.no_label.Visible = false;
            // 
            // close_box
            // 
            this.close_box.BackColor = System.Drawing.Color.Transparent;
            this.close_box.Location = new System.Drawing.Point(334, 3);
            this.close_box.Name = "close_box";
            this.close_box.Size = new System.Drawing.Size(21, 21);
            this.close_box.TabIndex = 12;
            this.close_box.TabStop = false;
            this.close_box.Tag = "close";
            this.close_box.Click += new System.EventHandler(this.pictureBox3_Click);
            this.close_box.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseDown);
            this.close_box.MouseEnter += new System.EventHandler(this.pictureBox2_MouseEnter);
            this.close_box.MouseLeave += new System.EventHandler(this.pictureBox2_MouseLeave);
            this.close_box.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseUp);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("宋体", 9F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(6, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "修改解锁密码";
            // 
            // SetPwd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 241);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.close_box);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SetPwd";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SetPwd";
            this.Load += new System.EventHandler(this.SetPwd_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.btn_ok)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_cancel)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.close_box)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox text_old;
        private System.Windows.Forms.TextBox text_new;
        private System.Windows.Forms.TextBox text_renew;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox btn_ok;
        private System.Windows.Forms.PictureBox btn_cancel;
        private System.Windows.Forms.Label info;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox close_box;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label no_label;
    }
}