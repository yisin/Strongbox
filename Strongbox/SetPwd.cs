﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Strongbox
{
    public partial class SetPwd : Form
    {
        Utils u = new Utils();

        bool initpwd = false;
        public SetPwd()
        {
            InitializeComponent();
            this.BackgroundImage = u.GetImageBySkin("pix");
            close_box.Image = u.GetImageBySkin("close");
        }

        private void SetPwd_Load(object sender, EventArgs e)
        {
            if (Utils.lockPwd != null && Utils.lockPwd.Equals("1"))
            {
                initpwd = true;
                no_label.Visible = true;
                text_old.Enabled = false;
            }
            Color col = Color.FromArgb(255, 247, 247, 247);
            panel1.BackColor = col;
            btn_ok.BackgroundImage = u.GetImageBySkin("btn_ok");
            btn_cancel.BackgroundImage = u.GetImageBySkin("btn_cancel");
        }


        private void pictureBox2_MouseEnter(object sender, EventArgs e)
        {
            PictureBox box = (PictureBox)sender;
            String tag = box.Tag.ToString();
            box.Image = u.GetImageBySkin(tag, 1);
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            PictureBox box = (PictureBox)sender;
            String tag = box.Tag.ToString();
            box.Image = u.GetImageBySkin(tag);
        }

        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                PictureBox box = (PictureBox)sender;
                String tag = box.Tag.ToString();
                box.Image = u.GetImageBySkin(tag, 2);
            }
        }

        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            PictureBox box = (PictureBox)sender;
            String tag = box.Tag.ToString();
            box.Image = u.GetImageBySkin(tag, 1);
        }

        Point mouseOff;//鼠标移动位置变量
        bool leftFlag;//标签是否为左键
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mouseOff = new Point(-e.X, -e.Y); //得到变量的值
                leftFlag = true;                  //点击左键按下时标注为true;
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                Point mouseSet = Control.MousePosition;
                mouseSet.Offset(mouseOff.X, mouseOff.Y);  //设置移动后的位置
                Location = mouseSet;
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            if (leftFlag)
            {
                leftFlag = false;//释放鼠标后标注为false;
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            String oldPwd = text_old.Text;
            String newPwd = text_new.Text;
            String renewPwd = text_renew.Text;
            if (!initpwd)
            {
                if (String.IsNullOrEmpty(oldPwd))
                {
                    info.Text = "请输入旧密码";
                    return;
                } else {
                    oldPwd = YSTools.YSEncrypt.EncryptA(YSTools.YSEncrypt.GetMD5(oldPwd), Utils.abkey);
                    if (!Utils.lockPwd.Equals(oldPwd))
                    {
                        info.Text = "旧密码不正确";
                        return;
                    }
                }
            }
            if (newPwd.Length < 4)
            {
                info.Text = "密码不能少于4位";
                return;
            }
            else if (!newPwd.Equals(renewPwd))
            {
                info.Text = "两次密码输入不一致";
                return;
            }

            Utils.lockPwd = YSTools.YSEncrypt.EncryptA(YSTools.YSEncrypt.GetMD5(renewPwd), Utils.abkey);
            this.Dispose();
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
